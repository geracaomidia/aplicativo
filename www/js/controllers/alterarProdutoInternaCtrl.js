( function () {
  'use strict';

  angular.module( 'smartsales.controllers.alterarProdutoInternaCtrl', [] )

  .controller( 'alterarProdutoInternaCtrl', alterarProdutoInternaCtrl );

  /* Injeção de dependência */
  alterarProdutoInternaCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading",
    "$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window"
  ];

  /* Deprecated */
  function alterarProdutoInternaCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window ) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Altera Produto');

      $rootScope.produtoData = [];
      $rootScope.precoUnitSeparado = [];
      $rootScope.produto = [];

      $rootScope.produto = {
        cotacaoID: $localstorage.get("IDCOTACAO"),
        clienteID: $localstorage.get("IDCLIENTECOTACAO"),
        clienteNOME: $localstorage.get("NOMECLIENTECOTACAO"),
        produtoID: $localstorage.get("idPROD"),
        produtoQTD: "",
        totalPRODUTO: "0",
        produtoDESCONTO: "0",
        precoUNITARIO: "0"
      };

      //alert($localstorage.get("idPROD"));


      //Chama funcao buscaProdutoDados
      buscaProdutoDados();

    }//Fim onDeviceReady

    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    /*Função que lista todas as informações básicas de um produto*/
    function buscaProdutoDados() {

      //Mensagem após clicar em Buscar - loading
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&PRODUTOCOD="+ $localstorage.get('idPROD');

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTODADOS.php", vars)
        .done(function(data){


            //Loop para ler resultado
            for ( var i = 0; i < data.PRODUTODADOSRESULT.STRING.length; i++ ) {

              $rootScope.produtoData[ i ] = data.PRODUTODADOSRESULT.STRING[ i ].split( "," );
              
              $rootScope.produtoData[ i ] = $rootScope.produtoData[ i ].toString();
            }

            //Fecha a mensagem de loading
            $ionicLoading.hide();

            // Função para definir o preço uitário do produto
            precoUnitario();

            //Busca Informações de Estoque Detalhada
            //produtoEstoqueDetSemRE(idProduto);
          
          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    } //End buscaProdutoDados



    /* Função que lista o preço unitário de um produto*/
    function precoUnitario() {

      //Mensagem após clicar em Buscar - loading
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "50000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&PRODUTOCOD="+ $localstorage.get('idPROD');

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOPRECOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.PRODUTOPRECOSRESULT.STRING[0];            
              
            for ( var i = 0; i < data.PRODUTOPRECOSRESULT.STRING.length; i++ ) {
              
              //if ( data.PRODUTOPRECOSRESULT.STRING[i].split(",")[0] != "N" ) {
                
                var n = i - 1;
                
                var tabelaPreco = data.PRODUTOPRECOSRESULT.STRING[i].split(",")[0];

                //alert(tabelaPreco + "-" + $localstorage.get('TABELAID'));

                if (tabelaPreco == $localstorage.get('TABELAID')){

                    $rootScope.produto.precoUNITARIO = data.PRODUTOPRECOSRESULT.STRING[i].split(",")[2];
                    $localstorage.set('PRODVALOR',data.PRODUTOPRECOSRESULT.STRING[i].split(",")[2]);

                } else {

                  if ((i+1) == data.PRODUTOPRECOSRESULT.STRING.length){

                    $cordovaToast.showShortBottom("Produto não disponível para esta Tabela de Preço!");

                    //Vai para tela de itens cotacao
                    $state.go( "itensCotacao" );

                  }

                }
            } //End for

            //Fecha o Loading
            $ionicLoading.hide();

          
          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End precoUnitario;

    //funcao para calcular total do produto
    $scope.calculaTotalProduto = function() {

      if ( $rootScope.produto.produtoQTD && $scope.produto.precoUNITARIO) {
        
        var total = ( parseFloat( $rootScope.produto.produtoQTD ) * parseFloat( $scope.produto.precoUNITARIO )).toFixed( 2 );
        
        total = parseFloat( total ).toFixed( 2 );

        //Salva Total do Produto em localstorage
        $localstorage.set("TOTALPRODUTO", total);

        $rootScope.produto.totalPRODUTO = total;
      }

    } // end calculaTotalProduto()


    //Funcao cotacaoitemalterar
    $scope.cotacaoitemalterar = function() {

      //Se for Null significa que esta adicionando o item
      if($localstorage.get("ITEMID") == null){
        
        //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );

        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&IDCOTACAO=" + $localstorage.get('IDCOTACAO') +
                  "&IDPRODUTO=" + $localstorage.get('idPROD') +
                  "&PRECOUNITARIO=" + $rootScope.produto.precoUNITARIO +
                  "&QUANTIDADE=" + $rootScope.produto.produtoQTD +
                  "&DESCONTOPERC=" + $scope.produto.produtoDESCONTO;

        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/COTACAOITEMINCLUIR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.COTACAOITEMINCLUIRRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

                $cordovaToast.showShortBottom("Item inserido com Sucesso!");

                //Vai para tela de detalhe produto
                $state.go("itensCotacao");
             
            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();


              //Mensagem de Erro do WS
              var erro = data.COTACAOITEMINCLUIRRESULT.STRING[1];
              $cordovaToast.showShortBottom(erro);

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );
      
      //Senao esta editando um item
      } else {

        //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );

        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&IDCOTACAO=" + $localstorage.get('IDCOTACAO') +
                  "&IDITEM=" + $localstorage.get('ITEMID') +
                  "&IDPRODUTO=" + $localstorage.get('idPROD') +
                  "&PRECOUNITARIO=" + $rootScope.produto.precoUNITARIO +
                  "&QUANTIDADE=" + $rootScope.produto.produtoQTD +
                  "&DESCONTOPERC=" + $scope.produto.produtoDESCONTO;

        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/COTACAOITEMALTERAR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.COTACAOITEMALTERARRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

                //Fecha o Loading
                $ionicLoading.hide();

                $cordovaToast.showShortBottom("Item alterado com Sucesso!");

                //Vai para tela de detalhe produto
                $state.go("itensCotacao");
             
            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();


              //Mensagem de Erro do WS
              var erro = data.COTACAOITEMALTERARRESULT.STRING[1];
              $cordovaToast.showShortBottom(erro);

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );
      }

    } //End cotacaoItemAlterar();

  }

} )();