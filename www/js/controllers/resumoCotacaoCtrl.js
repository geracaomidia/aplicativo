( function () {
  'use strict';

  angular.module( 'smartsales.controllers.resumoCotacaoCtrl', [] )

  /* @Controller*/
  .controller( "resumoCotacaoCtrl", resumoCotacaoCtrl );

  /* Injeção de dependência*/
  resumoCotacaoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading","$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaDevice", "loginFactory", "$ionicActionSheet"
  ];

  /*Declara uma função para ser usado no controller 'resumoCotacaoCtrl' e com todas as funções do $scope Cliente*/
  function resumoCotacaoCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaDevice, loginFactory, $ionicActionSheet) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Resumo Cotação');


      $scope.cotResumo = [];

      //Chama função para carregar informações da cotação
      cotacaoResumo();


    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Funcao que busca dados de Cotacao Resumo
    function cotacaoResumo(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Cotação....',
        duration: "60000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAORESUMO.php", vars)
        .done(function(data){

          //alert(data.COTACAODADOSRESULT.STRING);

          //Recebe o status de retorno
          $scope.status = data.COTACAORESUMORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            $scope.cotResumo = data.COTACAORESUMORESULT.STRING;

            //Fecha Loading
            $ionicLoading.hide();


          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAORESUMORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Resumo


    //Gravar cotação
    function cotacaoPedidoGravarResumo(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOPEDIDOGRAVAR.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOPEDIDOGRAVARRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Cotação salva com Sucesso!");

            //Direciona apos conclusao
            $state.go("menuClientes");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Erro ao salvar a cotação!");


          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );


    }//End cotacaoPedidoGravarResumo


    /*Abre opções para cotação*/
    $scope.opcoesCotacao = function(idCotacao) {

      //Salva i ID da Cotação
      //$localstorage.set("IDCOTACAO", idCotacao);

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
            text: '<i class="fa  fa-floppy-o"></i> Salvar e Sair'
          },{
            text: '<i class="fa  fa-chevron-right"></i> Encerrar / Transmitir'
          },{
            text: '<i class="fa  fa-trash"></i> Excluir Cotação'
          }
        ],
        cancel: function () {
          
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {

            $state.go("menuClientes");

          } else if ( index == '1' ) {

            cotacaoPedidoGravarResumo();
            
          } else {

            excluirCotacao();

          }
          return true;
        }
      } );
    } //End opcoesCotacao;


    /*Função que permite a exclusão de uma cotação de um cliente*/
    function excluirCotacao() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOEXCLUIR.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOEXCLUIRRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Cotação excluída com Sucesso!");

            //Chama função para consultar cotações em aberto
          cotacaoEmAberto();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Erro ao excluir a cotação!");

            //Chama função para consultar cotações em aberto
            cotacaoEmAberto();

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } // End excluirCotacao();

}} )();