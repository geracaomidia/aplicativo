( function () {
  'use strict';

  angular.module( 'smartsales.controllers.detalheProdutoCtrl', [] )

  /* @Controller*/
  .controller( "detalheProdutoCtrl", detalheProdutoCtrl );

  /* Injeção de dependência*/
  detalheProdutoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$ionicPlatform", "$stateParams", "$ionicActionSheet", "$localstorage"
  ];

  /*  Declara uma função para ser usado no controller 'produtoCtrl' e com todas as funções do $scope Cliente*/
  function detalheProdutoCtrl($ionicPopup, $rootScope, $scope, $state, $http, $ionicLoading, $ionicPlatform, $stateParams, $ionicActionSheet, $localstorage ) {

    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Detalhe Produto');

      $rootScope.produtoData = [];
      $rootScope.produtoEstoque = [];
      $rootScope.precoUnitSeparado = [ {} ];

      var idProduto = $localstorage.get("idPROD");

      produtoEspecificoSemRe(idProduto);

    }//End onDeviceReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    /*Função que lista todas as informações básicas de um produto*/
    function produtoEspecificoSemRe(idProduto) {

      //Mensagem após clicar em Buscar - loading
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&PRODUTOCOD="+ idProduto;

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTODADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.PRODUTODADOSRESULT.STRING[0];

            //Loop para ler resultado
            for ( var i = 0; i < data.PRODUTODADOSRESULT.STRING.length; i++ ) {

              $rootScope.produtoData[ i ] = data.PRODUTODADOSRESULT.STRING[ i ].split( "," );
              
              $rootScope.produtoData[ i ] = $rootScope.produtoData[ i ].toString();
            }

            //Fecha a mensagem de loading
            $ionicLoading.hide();

            // Função para definir o preço uitário do produto
            precoUnitario( idProduto );

            //Busca Informações de Estoque Detalhada
            produtoEstoqueDetSemRE(idProduto);
          
          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    } //End produtoEspecificoSemRe


    /* Função que lista o preço unitário de um produto*/
    function precoUnitario( idProduto ) {

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&PRODUTOCOD="+ idProduto;

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOPRECOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.PRODUTOPRECOSRESULT.STRING[0];

            for ( var i = 0; i < data.PRODUTOPRECOSRESULT.STRING.length; i++ ) {
              
              $scope.precoUnit[ i ] = data.PRODUTOPRECOSRESULT.STRING[ i ].split( "," );
              
              if ( data.PRODUTOPRECOSRESULT.STRING[i].split(",")[0] != "N" ) {
                
                var n = i - 1;
                
                $rootScope.precoUnitSeparado[ n ] = {
                  id: data.PRODUTOPRECOSRESULT.STRING[i].split(",")[0],
                  nome: data.PRODUTOPRECOSRESULT.STRING[i].split(",")[1],
                  valor: data.PRODUTOPRECOSRESULT.STRING[i].split(",")[2]
                };

              } else {

                //Fecha o Loading
                $ionicLoading.hide();

                //Mensagem de Erro
                $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
              }
            } //End for

          
          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End precoUnitario;


    /* Função que lista as informações detalhadas de um produto*/
    function produtoEstoqueDetSemRE( idProduto ) {


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&PRODUTOCOD="+ idProduto;

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOESTOQUE.php", vars)
        .done(function(data){

            for ( var i = 0; i < data.PRODUTOESTOQUERESULT.STRING.length; i++ ) {
              
              $rootScope.produtoEstoque[ i ] = data.PRODUTOESTOQUERESULT.STRING[ i ].split( "," );
              
              $rootScope.produtoEstoque[ i ] = $rootScope.produtoEstoque[ i ].toString();
            }

            // Função para definir o preço uitário do produto
            precoUnitario( idProduto );
            if ( !$rootScope.precoUnitSeparado[ 0 ].valor ) {
              $rootScope.precoUnitSeparado[ 0 ].valor = "Sem valor!";
            }

            //Fecha mensagem de loading
            $ionicLoading.hide();

        } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End produtoEstoqueDet


  } //End controller

} )();
