( function () {
  'use strict';

  angular.module( 'smartsales.controllers.negociosCtrl', [] )

  .controller( 'negociosCtrl', negociosCtrl );

  /* Injeção de dependência */
  negociosCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading","$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaGeolocation","$ionicActionSheet"
  ];

  /* Deprecated */
  function negociosCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaGeolocation,$ionicActionSheet ) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Menu Negócios');

      $scope.showAlert = showAlert;
      $scope.cotacaoEmAbertoEspecifico = [];

      //Chama funcao para listar cotacoes
      listaCotacoesVendedor();

    }//Fim onDevideReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Função listarProdutos
    function listaCotacoesVendedor() {
      
      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "50000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&VENDCOD="+ $localstorage.get('VENDCOD');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOVENDABERTO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOVENDABERTORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.COTACAOVENDABERTORESULT.STRING.length; i++ ) {

              var n = i -1;

              $scope.cotacaoEmAbertoEspecifico[n] = {
                idCotacao: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[0],
                emissao: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[1],
                idCliente: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[2],
                fantasia: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[3],
                totalGeral: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[4],
                qtdItens: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[5]
              };
            }

            //Fecha o loading
            $ionicLoading.hide();


          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOVENDABERTORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

        } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
      } );

      //OFFLINE


      // //Define array
      // $scope.cotacaoEmAberto = [];

      // //Recebe DB
      // var cotacaoVendedor = $localstorage.get("cotVendedorDB");

      // //Salva no array
      // $scope.cotacaoEmAberto = JSON.parse(cotacaoVendedor);

      // //Valida status
      // $scope.status = $scope.cotacaoEmAberto.STRING[0];

      // //Valida as condições
      // if ($scope.status == "S"){

      //   //Carrega informações de retorno
      //   for ( var i = 0; i < $scope.cotacaoEmAberto.STRING.length; i++ ) {

      //     var n = i -1;

      //     $scope.cotacaoEmAbertoEspecifico[n] = {
      //       idCotacao: $scope.cotacaoEmAberto.STRING[i].split(",")[0],
      //       emissao: $scope.cotacaoEmAberto.STRING[i].split(",")[1],
      //       idCliente: $scope.cotacaoEmAberto.STRING[i].split(",")[2],
      //       fantasia: $scope.cotacaoEmAberto.STRING[i].split(",")[3],
      //       totalGeral: $scope.cotacaoEmAberto.STRING[i].split(",")[4],
      //       qtdItens: $scope.cotacaoEmAberto.STRING[i].split(",")[5]
      //     };
      //   }

      //   //Fecha o loading
      //   $ionicLoading.hide();

      // } else if ($scope.status == "N"){

      //   //Fecha o loading
      //   $ionicLoading.hide();
        
      //   //Mensagem de Erro do WS
      //   var erro = $scope.cotacaoEmAberto.STRING[1];
      //   $cordovaToast.showShortBottom(erro);

      //}//End IF Valida condição

      //END OFFLINE

    }//End listaCotacoesVendedor;


    /*Abre opções para cotação*/
    $scope.opcoesCotacao = function(idCotacao,idCliente) {

      //Salva i ID da Cotação
      $localstorage.set("IDCOTACAO", idCotacao);
      $localstorage.set("idCliente", idCliente);

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
            text: '<i class="fa  fa-chevron-right"></i> Encerrar / Transmitir'
          },
          {
            text: '<i class="fa  fa-edit"></i> Alterar Cotação'
          }, {
            text: '<i class="fa  fa-times"></i> Excluir cotação'
          }
        ],
        cancel: function () {
          
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {

            //Chama funcao para gravar cotação
            cotacaoPedidoGravar();

          } else if ( index == '1' ) {

            //Chama funcao para listar cotacao
            cotacaoDadosListar();

          } else {

            //Chama funcao para excluir cotacao
            excluirCotacao();

          }
          return true;
        }
      } );
    } //End opcoesCotacao;


     /*Função que transforma uma cotação em pedido de venda*/
    function cotacaoPedidoGravar() {

      //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );


        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

        
        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/COTACAOPEDIDOGRAVAR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.COTACAOPEDIDOGRAVARRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

              //Fecha o Loading
              $ionicLoading.hide();

              $cordovaToast.showShortBottom("Cotação salva com Sucesso!");

              //Chama função para consultar cotações em aberto
              cotacaoEmAberto();

            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              $cordovaToast.showShortBottom("Erro ao salvar a cotação!");

              //Chama função para consultar cotações em aberto
              cotacaoEmAberto();

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );

    } // End cotacaoPedidoGravar;


    /*Função que permite a exclusão de uma cotação de um cliente*/
    function excluirCotacao() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOEXCLUIR.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOEXCLUIRRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Cotação excluída com Sucesso!");

            //Chama função para consultar cotações em aberto
          cotacaoEmAberto();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Erro ao excluir a cotação!");

            //Chama função para consultar cotações em aberto
            cotacaoEmAberto();

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } // End excluirCotacao();

    //Função cotacaoDados
    function cotacaoDadosListar(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Cotação....',
        duration: "6000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAODADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAODADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Salva em localstorage informações da cotacao
            $localstorage.set("DESCONTO1", data.COTACAODADOSRESULT.STRING[7]);
            $localstorage.set("DESCONTO2", data.COTACAODADOSRESULT.STRING[8]);
            $localstorage.set("DESCONTO3", data.COTACAODADOSRESULT.STRING[9]);
            $localstorage.set("DESCONTO4", data.COTACAODADOSRESULT.STRING[10]);

            $localstorage.set("acaoCOTACAO", "Editar");

            //Fecha o Loading
            $ionicLoading.hide();

            //Direciona para revisar de cotacao
            $state.go("addCotacao");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAODADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Dados
  }

} )();