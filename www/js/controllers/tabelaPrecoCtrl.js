( function () {
  'use strict';

  angular.module( 'smartsales.controllers.tabelaPrecoCtrl', [ 'ngCordova', "ui.router" ] )

  /* @Controller*/
  .controller( "tabelaPrecoCtrl", tabelaPrecoCtrl );

  /* Injeção de dependência*/
  tabelaPrecoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$cordovaBarcodeScanner", "$ionicPlatform", "$stateParams", "$ionicActionSheet", "$localstorage", "$ionicModal", "$cordovaToast"
  ];

  /*  Declara uma função para ser usado no controller 'tabelaPreco' e com todas as funções do $scope Cliente*/
  function tabelaPrecoCtrl($ionicPopup, $rootScope, $scope, $state, $http, $ionicLoading, $cordovaBarcodeScanner, $ionicPlatform, $stateParams, $ionicActionSheet, $localstorage, $cordovaToast ) {

    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Tabela Preços');


      $scope.showAlert = showAlert;
      $scope.separadoItem = [];
      $scope.estado = "";

      tabelaPrecos();

    }//End onDeviceReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Função tabelaPrecos
    function tabelaPrecos() {
      
      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&PRODUTOCOD="+ $localstorage.get('idPROD');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOPRECOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.PRODUTOPRECOSRESULT.STRING[0];


          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.PRODUTOPRECOSRESULT.STRING.length; i++ ) {

              var n = i -1;

              $scope.separadoItem[n] = {
                cod: data.PRODUTOPRECOSRESULT.STRING[i].split(",")[0],
                tabela: data.PRODUTOPRECOSRESULT.STRING[i].split(",")[1],
                valor: data.PRODUTOPRECOSRESULT.STRING[i].split(",")[2]
              };
            }

            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.PRODUTOPRECOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );


    }//End listarProdutos();

    $scope.carregaImposto = function(){
      var estado = window.document.getElementById("estado").value;
      var itemValor = window.document.getElementById("itemValor").value;

      if (estado == "? string: ?"){

        $cordovaToast.showShortBottom("Selecione um estado!");

      } else {

        $localstorage.set("PRODESTADO", estado);
        $localstorage.set("PRODVALOR", itemValor);
        $localstorage.set("TIPOCLIENTE", "R");

        //Vai para tela de imposto
        $state.go("imposto");

      }
    }

  } //End controller

} )();
