( function () {
  'use strict';

  angular.module( 'smartsales.controllers.listaCotacaoVendedorCtrl', [ 'ngCordova', "ui.router" ] )

  /* @Controller*/
  .controller( "listaCotacaoVendedorCtrl", listaCotacaoVendedorCtrl );

  /* Injeção de dependência*/
  listaCotacaoVendedorCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$ionicPlatform", "$stateParams", "$ionicActionSheet", "$localstorage"
  ];

  /*  Declara uma função para ser usado no controller 'produtoCtrl' e com todas as funções do $scope Cliente*/
  function listaCotacaoVendedorCtrl($ionicPopup, $rootScope, $scope, $state, $http, $ionicLoading,$ionicPlatform, $stateParams, $ionicActionSheet, $localstorage ) {

    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      //window.analytics.startTrackerWithId('UA-40700062-2');
      //window.analytics.trackView('Lista Cotações Vendedor');

      $scope.showAlert = showAlert;
      $scope.cotacaoEmAbertoEspecifico = [];

      //Chama funcao para listar cotacoes
      listaCotacoesVendedor();

    }//End onDeviceReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Função listarProdutos
    function listaCotacoesVendedor() {
      
      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&VENDCOD="+ $localstorage.get('VENDCOD');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOVENDABERTO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOVENDABERTORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.COTACAOVENDABERTORESULT.STRING.length; i++ ) {

              var n = i -1;

              $scope.cotacaoEmAbertoEspecifico[n] = {
                idCotacao: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[0],
                emissao: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[1],
                idCliente: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[2],
                fantasia: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[3],
                totalGeral: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[4],
                qtdItens: data.COTACAOVENDABERTORESULT.STRING[i].split(",")[5]
              };
            }

            //Fecha o loading
            $ionicLoading.hide();


          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOVENDABERTORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

        } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
      } );

      //OFFLINE


      // //Define array
      // $scope.cotacaoEmAberto = [];

      // //Recebe DB
      // var cotacaoVendedor = $localstorage.get("cotVendedorDB");

      // //Salva no array
      // $scope.cotacaoEmAberto = JSON.parse(cotacaoVendedor);

      // //Valida status
      // $scope.status = $scope.cotacaoEmAberto.STRING[0];

      // //Valida as condições
      // if ($scope.status == "S"){

      //   //Carrega informações de retorno
      //   for ( var i = 0; i < $scope.cotacaoEmAberto.STRING.length; i++ ) {

      //     var n = i -1;

      //     $scope.cotacaoEmAbertoEspecifico[n] = {
      //       idCotacao: $scope.cotacaoEmAberto.STRING[i].split(",")[0],
      //       emissao: $scope.cotacaoEmAberto.STRING[i].split(",")[1],
      //       idCliente: $scope.cotacaoEmAberto.STRING[i].split(",")[2],
      //       fantasia: $scope.cotacaoEmAberto.STRING[i].split(",")[3],
      //       totalGeral: $scope.cotacaoEmAberto.STRING[i].split(",")[4],
      //       qtdItens: $scope.cotacaoEmAberto.STRING[i].split(",")[5]
      //     };
      //   }

      //   //Fecha o loading
      //   $ionicLoading.hide();

      // } else if ($scope.status == "N"){

      //   //Fecha o loading
      //   $ionicLoading.hide();
        
      //   //Mensagem de Erro do WS
      //   var erro = $scope.cotacaoEmAberto.STRING[1];
      //   $cordovaToast.showShortBottom(erro);

      //}//End IF Valida condição

      //END OFFLINE

    }//End listaCotacoesVendedor;


  } //End controller

} )();
