( function () {
  'use strict';

  angular.module( 'smartsales.controllers.itensCotacaoCtrl', [] )

  /* @Controller*/
  .controller( "itensCotacaoCtrl", itensCotacaoCtrl );

  /* Injeção de dependência*/
  itensCotacaoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading","$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaDevice", "$ionicActionSheet"
  ];

  /*Declara uma função para ser usado no controller 'itensCotacaoCtrl' e com todas as funções do $scope Cliente*/
  function itensCotacaoCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaDevice, $ionicActionSheet) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    // //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Itens Cotação');

      $rootScope.cotacaoListaItensSeparado = [];

      $rootScope.cotacao = {
        id: $localstorage.get("IDCOTACAO"),
        nomeCliente: $localstorage.get("NOMECLIENTECOTACAO")
      }

      cotacaoDados();

      //Chama função para listar itens da cotação
      cotacaoListaTodosItens();

    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    $scope.doRefreshItensCotacao = function () {
      cotacaoListaTodosItens();
      $scope.$broadcast('scroll.refreshComplete');
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
    };


    /* Função que lista todos os itens default da cotação */
    function cotacaoListaTodosItens() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get("IDCOTACAO");

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOLISTAITENS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOLISTAITENSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

              //Carrega informações de retorno
              for ( var i = 0; i < data.COTACAOLISTAITENSRESULT.STRING.length; i++ ) {

                var n = i -1;

                $rootScope.cotacaoListaItensSeparado[ n ] = {
                  item: data.COTACAOLISTAITENSRESULT.STRING[i].split(",")[0],
                  idProduto: data.COTACAOLISTAITENSRESULT.STRING[i].split(",")[1],
                  quantidade:data.COTACAOLISTAITENSRESULT.STRING[i].split(",")[2],
                  valorTotal: data.COTACAOLISTAITENSRESULT.STRING[i].split(",")[4],
                  descricaoProduto: data.COTACAOLISTAITENSRESULT.STRING[i].split(",")[5]
                };
              }

            //Fecha Loading
            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            $cordovaToast.showShortBottom("Nenhum item foi adicionado para esta cotação!");

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End cotacaoListaTodosItens();

    /*Abre um menu ao clicar em itens da cotação com opções de excluir & alterar*/
    $scope.actionMenuItensCotacao = function(itemId) {

      $localstorage.set("ITEMID", itemId);

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
          text: '<i class="fa fa-edit"></i>  Alterar item'
        }, {
          text: '<i class="fa fa-times"></i> Excluir item'
        } ],
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {
            
            //Vai para tela de detalhe produto
            $state.go("alterarProdutoInterna");

          }
          if ( index == '1' ) {
            cotacaoItemExcluir();
            //    cotacaoListaTodosItens();
          }
          return true;
        }
      } );
    } //End actionMenuItensCotacao


    //Funcao para excluir item
    function cotacaoItemExcluir(){

        //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );

        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&IDCOTACAO=" + $localstorage.get('IDCOTACAO') +
                  "&IDITEM=" + $localstorage.get('ITEMID');

        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/COTACAOITEMEXCLUIR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.COTACAOITEMEXCLUIRRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

                //Fecha o Loading
                $ionicLoading.hide();

                $cordovaToast.showShortBottom("Item excluído com Sucesso!");

                //Chama função para listar itens da cotação
                cotacaoListaTodosItens();
             
            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro do WS
              var erro = data.COTACAOITEMEXCLUIRRESULT.STRING[1];
              $cordovaToast.showShortBottom(erro);

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );
      }//End exluiritemcotacao

      //Funcao finaliza cotacao
      $scope.finalizaCotacao = function (id){

      //Mensagem após clicar em Buscar
        // $ionicLoading.show( {
        //   template: '<ion-spinner icon="dots"></ion-spinner>',
        //   duration: "5000"
        // } );


        $state.go("resumoCotacao");

        // //Configura variaveis do POST
        // var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
        //           "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
        //           "&IDCOTACAO="+ id;

        
        // //Faz POST no WS
        // $.post("http://geracaomidia.com.br/app/smartsales/COTACAOPEDIDOGRAVAR.php", vars)
        //   .done(function(data){

        //     //Recebe o status de retorno
        //     $scope.status = data.COTACAOPEDIDOGRAVARRESULT.STRING[0];

        //     //Valida as condições
        //     if ($scope.status == "S"){

        //       //Fecha o Loading
        //       $ionicLoading.hide();

        //       $cordovaToast.showShortBottom("Cotação finalizada com Sucesso!");

        //       //Vai para tela de detalhe produto
        //       $state.go("menuClientes");


        //     } else if ($scope.status == "N"){

        //       //Fecha o Loading
        //       $ionicLoading.hide();

        //       $cordovaToast.showShortBottom("Erro ao finalizar a cotação!");

        //       //Vai para tela de detalhe produto
        //       $state.go("menuClientes");

        //     }//End IF Valida condição

        //     } )
        //     .fail(function(data) {

        //       //Fecha o Loading
        //       $ionicLoading.hide();

        //       //Mensagem de Erro
        //       $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        //   } );
      }//End finalizaCotacao

    //Função cotacaoDados
    function cotacaoDados(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Cotação....',
        duration: "6000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAODADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAODADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            $rootScope.totalCotacao = data.COTACAODADOSRESULT.STRING[13];


            //Fecha Loading
            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAODADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Dados


    /*Abre opções para cotação*/
    $scope.opcoesCotacao = function(idCotacao) {

      //Salva i ID da Cotação
      //$localstorage.set("IDCOTACAO", idCotacao);

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
            text: '<i class="fa  fa-clock-o"></i> Resumo Cotação'
          },{
            text: '<i class="fa  fa-percent"></i> Descontos'
          },{
            text: '<i class="fa  fa-edit"></i> Excluir Cotação'
          },{
            text: '<i class="fa  fa-floppy-o"></i> Salvar e Sair'
          }
        ],
        cancel: function () {
          
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {

            //Chama funcao para gravar cotação
            //cotacaoPedidoGravar();
            $state.go("resumoCotacao");

          } else if ( index == '1' ) {

            //Chama funcao para listar cotacao
            //cotacaoDadosListar();
            $state.go("descontos");

          } else if ( index == '2' ) {

            //$state.go("descontos");
            excluirCotacao();

          } else {

            $state.go("menuClientes");

          }
          return true;
        }
      } );
    } //End opcoesCotacao;


    /*Função que transforma uma cotação em pedido de venda*/
    function cotacaoPedidoGravar() {

      //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );


        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

        
        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/COTACAOPEDIDOGRAVAR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.COTACAOPEDIDOGRAVARRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

              //Fecha o Loading
              $ionicLoading.hide();

              $cordovaToast.showShortBottom("Cotação salva com Sucesso!");

              //Chama função para consultar cotações em aberto
              //cotacaoEmAberto();
              $state.go("menuClientes");

            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              $cordovaToast.showShortBottom("Erro ao salvar a cotação!");

              //Chama função para consultar cotações em aberto
              //cotacaoEmAberto();

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );

    } // End cotacaoPedidoGravar;


    /*Função que permite a exclusão de uma cotação de um cliente*/
    function excluirCotacao() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOEXCLUIR.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOEXCLUIRRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Cotação excluída com Sucesso!");

            //Chama função para consultar cotações em aberto
            //cotacaoEmAberto();
            $state.go("menuClientes");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Erro ao excluir a cotação!");

            //Chama função para consultar cotações em aberto
            //cotacaoEmAberto();

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } // End excluirCotacao();

    //Função cotacaoDados
    function cotacaoDadosListar(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Cotação....',
        duration: "6000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAODADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAODADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Salva em localstorage informações da cotacao
            $localstorage.set("DESCONTO1", data.COTACAODADOSRESULT.STRING[7]);
            $localstorage.set("DESCONTO2", data.COTACAODADOSRESULT.STRING[8]);
            $localstorage.set("DESCONTO3", data.COTACAODADOSRESULT.STRING[9]);
            $localstorage.set("DESCONTO4", data.COTACAODADOSRESULT.STRING[10]);

            $localstorage.set("acaoCOTACAO", "Editar");

            //Fecha o Loading
            $ionicLoading.hide();

            //Direciona para revisar de cotacao
            $state.go("addCotacao");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAODADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Dados

}} )();