( function () {
  'use strict';

  angular.module( 'smartsales.controllers.configCtrl', [] )

  .controller( 'configCtrl', configCtrl );

  /* Injeção de dependência */
  configCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading",
    "$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaGeolocation"
  ];

  /* Deprecated */
  function configCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaGeolocation ) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      // window.analytics.startTrackerWithId('UA-40700062-2');
      // window.analytics.trackView('Configuração de Login');

      //Valida se já existe algum vendedor salvo em localstorage
      if ($localstorage.get("vendedor") == "" && $localstorage.get("senha")==""){

        //Inicia o root configuracao sem Licenca
        $rootScope.configuracao = {
          servidor: $localstorage.get("servidor"),
          porta: $localstorage.get("porta"),
          licenca: ""
        };

      } else {

        //Inicia o root configuracao com Licenca
        $rootScope.configuracao = {
          servidor: $localstorage.get("servidor"),
          porta: $localstorage.get("porta"),
          licenca: $localstorage.get("licenca")
        }; 

      }

    }//Fim onDevideReady

    //Salva as configurações
    $scope.salvarConfig = function(){

      //Valida se campo licença esta vazio
      if($scope.configuracao.licenca == ""){

        $cordovaToast.showShortBottom( 'Preencha o campo Licença!' );

      } else {

        //Salva as informações no localstorage
        $localstorage.set("licenca", $scope.configuracao.licenca);
        $cordovaToast.showShortBottom( 'Dados armazenados!' );

      }
    }//Fim salvarConfig
  }

} )();