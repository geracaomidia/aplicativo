( function () {

  angular.module( 'smartsales.controllers.actionSheetCtrl', [] )

  .controller( "actionSheetCtrl", actionSheetCtrl );

  /*
   * Injeção de dependência
   */
  actionSheetCtrl.$inject = [ "$scope", "$ionicActionSheet", "$timeout", "$state" ];

  function actionSheetCtrl( $scope, $ionicActionSheet, $timeout, $state ) {

    /*
     * Função que abre um menu com as opões de adicionar um Cliente ou uma Cotação
     */
    $scope.show = function () {

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
          text: '<i class="fa fa-users"></i> Clientes'
        }, {
          text: '<i class="fa fa-bar-chart"></i> Cotação'
        } ],
        titleText: '<h4>Adicionar</h4> <p>Selecione o item que deseja adicionar.</p>',
        cancelText: '<i class="fa fa-times"></i> Fechar',
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {
            $state.go( "addCliente" );
          } else {
            $state.go( "clientes" );
          }
          return true;
        }
      } );
    };
  }

} )();
