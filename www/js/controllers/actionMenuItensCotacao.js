( function () {
  'use strict';

  angular.module( 'smartsales.controllers.actionMenuItensCotacao', [] )

  // Show the action sheet itens Cotacao
  .controller( "actionMenuItensCotacao", actionMenuItensCotacao );

  /*
   * Injeção de dependência
   */
  actionMenuItensCotacao.$inject = [ "$scope", "$ionicActionSheet", "$timeout", "$state" ];

  function actionMenuItensCotacao( $scope, $ionicActionSheet, $timeout, $state ) {

    /*
     * Deprecated
     */
    $scope.show = function () {

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
          text: '<i class="fa fa-edit"></i>  Alterar item'
        }, {
          text: '<i class="fa fa-times"></i> Excluir item'
        } ],
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {
            alert( "teste" );

          } else {
            alert( "teste" );
          }
          return true;
        }
      } );
    };

  }

} )();
