( function () {
  'use strict';

  angular.module( 'smartsales.controllers.descontosCtrl', [] )

  /* @Controller*/
  .controller( "descontosCtrl", descontosCtrl );

  /* Injeção de dependência*/
  descontosCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading", "$state", "$cordovaToast", "$localstorage", "$window", "$cordovaDevice"
  ];

  /*Declara uma função para ser usado no controller 'descontosCtrl' e com todas as funções do $scope Cliente*/
  function descontosCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $cordovaToast, $localstorage, $window, $cordovaDevice) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Descontos');

      $rootScope.cotacao.clienteID = "";
      $rootScope.cotacao.pagtoID = "";
      $rootScope.cotacao.tabelaID = "";
      $rootScope.cotacao.desconto1 = "";
      $rootScope.cotacao.desconto2 = "";
      $rootScope.cotacao.desconto3 = "";
      $rootScope.cotacao.desconto4 = "";
      $rootScope.cotacao.obs = "";
      $rootScope.cotacao.transpID = "";
      $rootScope.cotacao.freteID = "";

      //chama função para carregar as informações
      cotacaoDados();

    }//Fim onDeviceReady


    //Função cotacaoDados
    function cotacaoDados(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Descontos Cotação....',
        duration: "60000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAODADOS.php", vars)
        .done(function(data){

          //alert(data.COTACAODADOSRESULT.STRING);

          //Recebe o status de retorno
          $scope.status = data.COTACAODADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){


            $rootScope.cotacao.clienteID = data.COTACAODADOSRESULT.STRING[3];
            $rootScope.cotacao.pagtoID = data.COTACAODADOSRESULT.STRING[5];
            $rootScope.cotacao.tabelaID = data.COTACAODADOSRESULT.STRING[7];
            $rootScope.cotacao.desconto1 = data.COTACAODADOSRESULT.STRING[9];
            $rootScope.cotacao.desconto2 = data.COTACAODADOSRESULT.STRING[10];
            $rootScope.cotacao.desconto3 = data.COTACAODADOSRESULT.STRING[11];
            $rootScope.cotacao.desconto4 = data.COTACAODADOSRESULT.STRING[12];
            $rootScope.cotacao.obs = data.COTACAODADOSRESULT.STRING[15];
            $rootScope.cotacao.transpID = data.COTACAODADOSRESULT.STRING[16];
            $rootScope.cotacao.freteID = data.COTACAODADOSRESULT.STRING[18];


            //Fecha Loading
            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAODADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Dados


    //Funcao que altera a cotação e salva os descontos informados
    $scope.alterarCotacao = function(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get("IDCOTACAO") +
                "&IDCLIENTE=" + $scope.cotacao.clienteID +
                "&IDCONDICAO=" + $rootScope.cotacao.pagtoID +
                "&IDTABELAPRECO=" + $rootScope.cotacao.tabelaID +
                "&IDTRANSPORTADORA=" + $rootScope.cotacao.transpID +
                "&TIPOFRETE=" + $rootScope.cotacao.freteID +
                "&DESCONTO1=" + $rootScope.cotacao.desconto1 +
                "&DESCONTO2=" + $rootScope.cotacao.desconto2 +
                "&DESCONTO3=" + $rootScope.cotacao.desconto3 +
                "&DESCONTO4=" + $rootScope.cotacao.desconto4 +
                "&OBSERVACOES=" + $rootScope.cotacao.obs;

                //alert(vars);

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOATUALIZA.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOATUALIZARESULT.STRING[0];


          //Valida as condições
          if ($scope.status == "S"){

            //Fecha Loading
            $ionicLoading.hide();

            //Mensagem
            $cordovaToast.showShortBottom("Cotação atualizada com Sucesso!");

            //Direciona para revisar de itens cotacao
            //$state.go("itensCotacao");
            $state.go("addCotacao");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOATUALIZARESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End alterarCotacao

}} )();