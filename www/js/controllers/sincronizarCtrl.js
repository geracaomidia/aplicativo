( function () {
  'use strict';

  angular.module( 'smartsales.controllers.sincronizarCtrl', [] )

  /* @Controller*/
  .controller( "sincronizarCtrl", sincronizarCtrl );

  /* Injeção de dependência*/
  sincronizarCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading","$state", "$cordovaToast", "$localstorage", "$window", "$cordovaDevice","$timeout"
  ];

  /*Declara uma função para ser usado no controller 'sincronizarCtrl' e com todas as funções do $scope Cliente*/
  function sincronizarCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $cordovaToast, $localstorage, $window, $cordovaDevice,$timeout) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    // //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      // window.analytics.startTrackerWithId('UA-40700062-2');
      // window.analytics.trackView('Sincronizar');

      document.getElementById("sincCotacaoVendedor").innerHTML = $localstorage.get("DATAsincCotacaoVendedor");
      document.getElementById("sincClientes").innerHTML = $localstorage.get("DATAsincClientes");
      document.getElementById("sincProdutos").innerHTML = $localstorage.get("DATAsincProdutos");

      $scope.dadosCliente = [];


    }//Fim onDeviceReady


    //Funcao que inicia a sincronização
    $scope.sincronizar = function(){

      //Sincroniza Cotacoes Vendedor
      cotacoesVendedor();

    }//End sincronizar


    //Sincroniza Cotações Vendedor
    function cotacoesVendedor(){

      //Mensagem após clicar em Entrar
      $ionicLoading.show( {
        template: 'Sincronizando Cotações Vendedor....',
        duration: "6000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&VENDCOD="+ $localstorage.get('VENDCOD');
      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOVENDABERTO.php", vars)
        .done(function(data){

            //Recebe o resultado

            //Converte
            var cotacaoVendedor = JSON.stringify(data.COTACAOVENDABERTORESULT);

            //Salva no DB
            $localstorage.set("cotVendedorDB", cotacaoVendedor);

            //Valida Data
            var data = new Date();

            $localstorage.set("DATAsincCotacaoVendedor", data.toLocaleString());
            document.getElementById("sincCotacaoVendedor").innerHTML = data.toLocaleString();

            //Fecha o loading
            $ionicLoading.hide();

            //Chama função para sincronizar clientes
            clientes();

        } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
      } );

    }//End cotacoesVendedor


    //Sincroniza Clientes
    function clientes(){

      //Mensagem após clicar em Entrar
      $ionicLoading.show( {
        template: 'Sincronizando Clientes....',
        duration: "60000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&VENDCOD=" + $localstorage.get('VENDCOD') + 
                  "&CLIENTEPESQ=a";

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/CLIENTEBUSCAR.php", vars)
        .done(function(data){

            //Recebe o resultado

            //Converte
            var clientes = JSON.stringify(data.CLIENTEBUSCARRESULT);

            //Salva no DB
            $localstorage.set("clientesDB", clientes);

            //Valida Data
            var data = new Date();

            $localstorage.set("DATAsincClientes", data.toLocaleString());
            document.getElementById("sincClientes").innerHTML = data.toLocaleString();

            //Fecha o loading
            $ionicLoading.hide();

          //Chama função para sincronizar Dados dos Clientes
          produtos();
        
        } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
      } );

    }//End clientes


    //Sincroniza cliente dados
    // function clienteDados(){

    //   //Mensagem após clicar em Entrar
    //   $ionicLoading.show( {
    //     template: 'Sincronizando Dados de Clientes....',
    //     duration: "60000"
    //   } );

    //   $scope.clienteInfo = [];

    //   //Recebe DB
    //   var cliInfo = $localstorage.get("clientesDB");

    //   //Salva no array
    //   $scope.clienteInfo = JSON.parse(cliInfo);

    //   //Valida status
    //   //$scope.status = $scope.clienteInfo.STRING[0];

    //   //Define array detalhes

    //   //alert($scope.status);
    //   //alert($scope.clienteInfo.STRING.length);

    //   //Valida as condições
    //   //if ($scope.status == "S"){

    //     //Carrega informações de retorno
    //     for ( var i = 0; i < $scope.clienteInfo.STRING.length; i++ ) {

            
    //         //alert($scope.clienteInfo.STRING[i].split(",")[0]);

    //         //Configura variaveis do POST
    //         var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
    //               "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
    //               "&IDCLIENTE=" + $scope.clienteInfo.STRING[i].split(",")[0]; 


    //             //alert(vars);
    //             var x = 1;

    //         //Faz POST no WS
    //         $.post("http://geracaomidia.com.br/app/smartsales/CLIENTEDADOS.php", vars)
    //         .done(function(data){

    //           //Recebe o status de retorno

    //           $scope.dadosCliente[x] = JSON.stringify(data.CLIENTEDADOSRESULT.STRING);

    //           // $scope.dadosCliente[x] = {
    //           //   id: data.CLIENTEDADOSRESULT.STRING[1].toString(),
    //           //   nome: data.CLIENTEDADOSRESULT.STRING[3].toString(),
    //           //   razao: data.CLIENTEDADOSRESULT.STRING[2].toString(),
    //           //   endereco: data.CLIENTEDADOSRESULT.STRING[4].toString(),
    //           //   numero: data.CLIENTEDADOSRESULT.STRING[5].toString(),
    //           //   complemento: data.CLIENTEDADOSRESULT.STRING[6].toString(),
    //           //   bairro: data.CLIENTEDADOSRESULT.STRING[7].toString(),
    //           //   cidade: data.CLIENTEDADOSRESULT.STRING[8].toString(),
    //           //   estado: data.CLIENTEDADOSRESULT.STRING[9].toString(),
    //           //   telefone: data.CLIENTEDADOSRESULT.STRING[10].toString(),
    //           //   email: data.CLIENTEDADOSRESULT.STRING[11].toString(),
    //           //   cnpj: data.CLIENTEDADOSRESULT.STRING[12].toString(),
    //           //   ie: data.CLIENTEDADOSRESULT.STRING[13].toString(),
    //           //   vendedor: data.CLIENTEDADOSRESULT.STRING[14].toString(),
    //           //   observacoes: data.CLIENTEDADOSRESULT.STRING[15].toString(),
    //           //   tipo: data.CLIENTEDADOSRESULT.STRING[16].toString()
    //           // };

    //           // //Valida as condições
    //           // if ($scope.status1 == "S"){

    //           //   $scope.dadosCliente[i] = data.CLIENTEDADOSRESULT.STRING.toString();

    //           //   //Fecha o Loading
    //           //   $ionicLoading.hide();

    //           // } else if ($scope.status == "N"){

    //           //   //Fecha o Loading
    //           //   $ionicLoading.hide();

    //           //   //Mensagem de Erro do WS
    //           //   var erro = data.CLIENTEDADOSRESULT.STRING[1];
    //           //   $cordovaToast.showShortBottom(erro);

    //           // }//End IF Valida condição
    //           x++;
    //         })
    //         .fail(function(data) {

    //             //Fecha o Loading
    //             $ionicLoading.hide();

    //             //Mensagem de Erro
    //             $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
    //         } );  

    //     }//End FOR

    //     alert($scope.dadosCliente.length);


    //     //Fecha o loading
    //     $ionicLoading.hide();

    //     //Chama função para buscar produtos
    //     produtos();

    //   //} else if ($scope.status == "N"){

    //     //Fecha o loading
    //     //$ionicLoading.hide();

    //     //Mensagem de Erro do WS
    //     //var erro = $scope.clienteInfo.STRING[1];
    //     //$cordovaToast.showShortBottom(erro);

    //   //}//End IF Valida condição

    // }//End clienteDados


    //Função para sincronizar Produtos
    function produtos(){

      //Mensagem após clicar em Entrar
      $ionicLoading.show( {
        template: 'Sincronizando Produtos....',
        duration: "60000"
      } );

      //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                  "&PRODUTOPESQ=a";

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOBUSCAR.php", vars)
        .done(function(data){

            //Recebe o resultado

            //Converte
            var produtos = JSON.stringify(data.PRODUTOBUSCARRESULT);

            //Salva no DB
            $localstorage.set("produtosDB", produtos);

            //Valida Data
            var data = new Date();

            $localstorage.set("DATAsincProdutos", data.toLocaleString());
            document.getElementById("sincProdutos").innerHTML = data.toLocaleString();

            //Fecha o loading
            $ionicLoading.hide();

        
        } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
      } );

    }//End Produtos

}} )();