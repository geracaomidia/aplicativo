( function () {
  'use strict';

  angular.module( 'smartsales.controllers.impostoCtrl', [] )

  /* @Controller*/
  .controller( "impostoCtrl", impostoCtrl );

  /* Injeção de dependência*/
  impostoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading","$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaDevice",
  ];

  /*Declara uma função para ser usado no controller 'impostoCtrl' e com todas as funções do $scope Cliente*/
  function impostoCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaDevice) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    // //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Imposto Produto');

      $rootScope.impostoLista = [];

      //Chama funcao para buscar imposto
      buscaImposto();

    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    function buscaImposto(){

      //Mensagem após clicar em Entrar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      if ($localstorage.get('idCliente') == null || $localstorage.get('idCliente') == ""){

        var idClienteImposto = "";

      } else {

        var idClienteImposto = $localstorage.get('idCliente');

      }


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                  "&PRODUTOCOD="+ $localstorage.get('idPROD')+
                  "&ESTADO="+ $localstorage.get('PRODESTADO')+
                  "&PRECOUNITARIO=" + $localstorage.get('PRODVALOR')+
                  "&IDCLIENTE=" + idClienteImposto;

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOIMPOSTO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.PRODUTOIMPOSTORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.PRODUTOIMPOSTORESULT.STRING.length; i++ ) {

              $rootScope.impostoLista[ i ] = data.PRODUTOIMPOSTORESULT.STRING[ i ].split( "," );
              
              $rootScope.impostoLista[ i ] = $rootScope.impostoLista[ i ].toString();
            }

            //Fecha o Loading
            $ionicLoading.hide();


          } else if (status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.PRODUTOIMPOSTORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

        } )
        .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//Fim buscaImposto

}} )();