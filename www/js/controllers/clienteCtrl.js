( function () {
  // 'use strict';

  //    /*CordovaToast, barcodeScanner plugins required!*/
  angular.module( 'smartsales.controllers.clienteCtrl', [ "ui.router", "ngCordova" ] )

  /*
   * @Controller
   */
  .controller( "clienteCtrl", clienteCtrl );

  /*
   * Injeção de dependência
   */
  clienteCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http",
    "$ionicLoading", "$state", "$ionicActionSheet", "$q", "$timeout",
    "loginFactory", "soapFactory", "cotacaoFactory", "$routeParams", "$stateParams", "$cordovaToast"
  ];

  /*
   * Declara uma função para ser usado no controller 'clienteCtrl' e com todas as funções do $scope Cliente
   */
  function clienteCtrl( $ionicPopup, $rootScope, $scope, $http,
    $ionicLoading, $state, $ionicActionSheet, $q, $timeout,
    loginFactory, soapFactory, cotacaoFactory, $routeParams, $stateParams, $cordovaToast ) {

    //Variables
    var pageLoad = true;
    $scope.data = {
      IDEMPRESA: loginFactory.idEmpresa,
      IDCONEXAO: loginFactory.idconexao,
      VENDCOD: loginFactory.vendcod,
      clienteId: $stateParams.clienteId,
      cotacaoId: $stateParams.cotacaoId,
      produtoId: $stateParams.produtoId,
      OBSERVACOES: cotacaoFactory.observacao,
      desconto1: "0",
      desconto2: "0",
      desconto3: "0",
      desconto4: "0"
    };

    $rootScope.cotacaoDados = {
      itemId: cotacaoFactory.itemId,
      produtoId: cotacaoFactory.produtoId,
      idCotacao: cotacaoFactory.idCotacao,
      idCliente: cotacaoFactory.idCliente,
      nomeCliente: cotacaoFactory.nomeCliente,
      condPgmt: {
        id: cotacaoFactory.condPgmt.id,
        nome: cotacaoFactory.condPgmt.nome
      },
      tabelaPreco: {
        id: cotacaoFactory.tabelaPreco.id,
        nome: cotacaoFactory.tabelaPreco.nome
      },
      transportadora: {
        id: cotacaoFactory.transportadora.id,
        nome: cotacaoFactory.transportadora.nome
      },
      tipoFrete: {
        id: cotacaoFactory.tipoFrete.id,
        nome: cotacaoFactory.tipoFrete.nome
      },
      desconto1: cotacaoFactory.desconto1,
      desconto2: cotacaoFactory.desconto2,
      desconto3: cotacaoFactory.desconto3,
      desconto4: cotacaoFactory.desconto4,
      observacoes: cotacaoFactory.observacao,
      idconexao: loginFactory.idconexao,
      idEmpresa: loginFactory.idEmpresa,
      produtoNome: cotacaoFactory.produtoNome,
      produtoEstoque: cotacaoFactory.produtoEstoque,
      produtoTransito: cotacaoFactory.produtoTransito,
      produtoQuantDisp: cotacaoFactory.produtoQuantDisp,
      produtoQuantCot: cotacaoFactory.produtoQuantCot,
      produtoPrecoUnit: cotacaoFactory.produtoPrecoUnit,
      produtoDescontoPerc: cotacaoFactory.produtoDescontoPerc,
      produtoValorTotal: cotacaoFactory.produtoValorTotal,
      listaItensSeparado: cotacaoFactory.listaItensSeparado
    };

    $scope.separado = [];

    $scope.separadoItem = [];
    $scope.clienteAtualizaDados = [];

    $rootScope.clienteDados = [];
    $rootScope.cotacaoEmAberto = [];
    $rootScope.cotacaoEmAbertoEspecifico = [];
    $rootScope.cotacaoListaItensSeparado = [];
    $rootScope.transSeparado = [];
    $rootScope.condPgmtSeparado = [];
    $rootScope.condTabelaPrecosSeparado = [];
    $rootScope.cliFinanceiroSeparado = [];

    $scope.transp = [];
    $scope.trans = [];
    $scope.condPgmt = [];
    $scope.condTabelaPrecos = [];
    $scope.cotItemIncluir = [];
    $scope.cotPedidoGravar = [];
    $scope.cotacaoListaItens = [];
    $scope.cliPedidosNfs = [];

    $rootScope.cotResumo = [];
    $rootScope.cliFinanceiro = [];
    $scope.paraLista = [];
    $rootScope.paraListaSeparado = [];
    $rootScope.cliPedidosNfsSeparado = [];

    //Function calls -
    $scope.showAlert = showAlert;

    $scope.listarClientes = listarClientes;
    $scope.clienteEspecifico = clienteEspecifico;
    $scope.clienteAtualizaCadastro = clienteAtualizaCadastro;
    $scope.clientePedidosNfs = clientePedidosNfs;
    $scope.clienteFinanceiro = clienteFinanceiro;

    $scope.cotacaoEmAberto = cotacaoEmAberto;
    $scope.cotacaoIncluir = cotacaoIncluir;
    $scope.alterarCotacao = alterarCotacao;
    $scope.excluirCotacao = excluirCotacao;
    $scope.cotacaoTransportadora = cotacaoTransportadora;
    $scope.cotacaoCondPagamento = cotacaoCondPagamento;
    $scope.cotacaoTabelaPrecos = cotacaoTabelaPrecos;
    $scope.parametrosLista = parametrosLista;
    $scope.cotacaoListaTodosItens = cotacaoListaTodosItens;
    $scope.cotacaoDadosListar = cotacaoDadosListar;
    $scope.cotacaoItemIncluir = cotacaoItemIncluir;
    $scope.cotacaoPedidoGravar = cotacaoPedidoGravar;
    $scope.cotacaoResumo = cotacaoResumo;

    $scope.showActionSheet = showActionSheet;
    $scope.actionMenuAlterarCotacao = actionMenuAlterarCotacao;
    $scope.alterarCotacaoParam = alterarCotacaoParam;
    $scope.cotacaoItemExcluir = cotacaoItemExcluir;
    $scope.cotacaoitemalterar = cotacaoitemalterar;
    $scope.cotacaoPedidoGravarResumo = cotacaoPedidoGravarResumo;
    $scope.cotacaoDadosDescontos = cotacaoDadosDescontos;
    $scope.cotacaoTransportadoraSemId = cotacaoTransportadoraSemId;
    $scope.cotacaoVendAberto = cotacaoVendAberto;

    $scope.doRefreshItensCotacao = function () {
      cotacaoListaTodosItens();
      // Stop the ion-refresher from spinning
      $scope.$broadcast( 'scroll.refreshComplete' );
    };

    $scope.doRefreshMenu = function () {
      cotacaoEmAbertoParam();
      $scope.$broadcast( 'scroll.refreshComplete' );
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
    };

    /*
     * Abre um menu ao clicar nos 'Settings' em alterarCotacao com as opções de 'Itens da cotação', 'Descontos', 'Resumo da cotação', 'Ajuda'
     */
    function actionMenuAlterarCotacao() {

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
          text: 'Itens da Cotação'
        }, {
          text: 'Descontos'
        }, {
          text: 'Resumo da Cotação'
        }, {
          text: 'Ajuda'
        } ],
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {

            var deferred = $q.defer();
            deferred.resolve(
              $state.go( "itensCotacao", {
                clienteId: $scope.data.clienteId,
                cotacaoId: $scope.data.cotacaoId
              } )
            );
            var promise = deferred.promise;
            promise.then( function () {
              return cotacaoListaTodosItens();
            } );


          } else if ( index == '1' ) {
            $state.go( "descontos", {
              clienteId: $scope.data.clienteId,
              cotacaoId: $scope.data.cotacaoId
            } );
            cotacaoDadosDescontos();

          } else if ( index == '2' ) {
            $state.go( "resumoCotacao", {
              clienteId: $scope.data.clienteId,
              cotacaoId: $scope.data.cotacaoId
            } );
            cotacaoResumo();

          } else {
            // $state.go("");
            alert( "Ajuda" );
          }
          return true;
        }
      } );
    } //End actionMenuAlterarCotacao();

    /*
     * Abre um menu ao clicar em uma cotação com as opções 'Encerrar / Transmitir', 'Alterar cotação', 'Excluir Cotação'
     */
    function showActionSheet( idCotacao ) {

      console.log( "idCotacao 11 : " + idCotacao );
      cotacaoFactory.idCotacao = idCotacao;

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
            text: '<i class="fa  fa-chevron-right"></i> Encerrar / Transmitir'
          },
          // { text: '<i class="fa  fa-dot-circle-o"></i> Itens da cotação' },
          {
            text: '<i class="fa  fa-edit"></i> Alterar Cotação'
          }, {
            text: '<i class="fa  fa-times"></i> Excluir cotação'
          }
        ],
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {
            cotacaoPedidoGravar( cotacaoFactory.idCotacao );

            // } else if (index == '1') {
            //     $state.go("itensCotacao", {clienteId: $scope.data.clienteId,
            //                                cotacaoId: $rootScope.cotacaoDados.idCotacao});
            // cotacaoListaTodosItens();

          } else if ( index == '1' ) {

            cotacaoDadosListar( idCotacao );

            // $state.go("alterarCotacao", {clienteId: $scope.data.clienteId,
            //                              cotacaoId: $rootScope.cotacaoDados.idCotacao});
            // cotacaoTabelaPrecos();
            // cotacaoCondPagamento();
            // cotacaoTransportadora();
            // parametrosLista();
            // $state.go("alterarCotacao");
            // alterarCotacao();
          } else {
            excluirCotacao( cotacaoFactory.idCotacao );

          }
          return true;
        }
      } );
    } //End showActionSheet();

    /*
     * Função para construir um alerta user-friendly
     */
    function showAlert( title, msg, duration ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg,
        duration: duration
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }

    /*
     * Função que lista todas as informações da cotação
     */
    function cotacaoResumo() {

      var url = soapFactory.url + "/COTACAORESUMO.php";
      var soapAction = soapFactory.soapAction + "/COTACAORESUMO";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": $scope.data.cotacaoId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.COTACAORESUMORESULT.STRING.length; i++ ) {
            $rootScope.cotResumo[ i ] = data.COTACAORESUMORESULT.STRING[ i ].split( "," );
            console.log( "Cotacao Resumo Result: " + [ i ] + " = " + $rootScope.cotResumo[ i ] );
            $rootScope.cotResumo[ i ] = $rootScope.cotResumo[ i ].toString();
          }
          $ionicLoading.hide();
          if ( $rootScope.cotResumo[ 0 ] == "S" ) {
            //Go
          } else {
            $scope.showAlert( "Smartsales", $rootScope.cotResumo[ 1 ] );
            history.back();
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End cotacaoResumo();

    /*
     * Função que lista todos os itens default da cotação
     */
    function cotacaoListaTodosItens() {

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
      } );

      var url = soapFactory.url + "/COTACAOLISTAITENS.php";
      var soapAction = soapFactory.soapAction + "/COTACAOLISTAITENS";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": $scope.data.cotacaoId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.COTACAOLISTAITENSRESULT.STRING.length; i++ ) {
            $scope.cotacaoListaItens[ i ] = data.COTACAOLISTAITENSRESULT.STRING[ i ].split( "," );
            console.log( "listaItens" + [ i ] + " = " + $scope.cotacaoListaItens[ i ] );
            // $scope.cotacaoListaItens[i] = $scope.cotacaoListaItens[i].toString();
            var n = i - 1;
            cotacaoFactory.listaItensSeparado[ n ] = {
              item: $scope.cotacaoListaItens[ i ][ 0 ],
              idProduto: $scope.cotacaoListaItens[ i ][ 1 ],
              quantidade: $scope.cotacaoListaItens[ i ][ 2 ],
              // naoSei : $scope.cotacaoListaItens[i][3],
              valorTotal: $scope.cotacaoListaItens[ i ][ 4 ],
              descricaoProduto: $scope.cotacaoListaItens[ i ][ 5 ]
            };
            $rootScope.cotacaoListaItensSeparado[ n ] = {
              item: $scope.cotacaoListaItens[ i ][ 0 ],
              idProduto: $scope.cotacaoListaItens[ i ][ 1 ],
              quantidade: $scope.cotacaoListaItens[ i ][ 2 ],
              valorTotal: $scope.cotacaoListaItens[ i ][ 4 ],
              descricaoProduto: $scope.cotacaoListaItens[ i ][ 5 ]
            };
          }
          $ionicLoading.hide();
          if ( $scope.cotacaoListaItens[ 0 ] == "S" ) {
            //Go
            $ionicLoading.hide();
          } else {
            $cordovaToast.showShortBottom( $scope.cotacaoListaItens[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // $scope.showAlert($scope.cotacaoListaItens);
            $ionicLoading.hide();
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End cotacaoListaTodosItens();

    /*
     * Função que lista todos os clientes baseados na pesquisa do usuário
     */
    function listarClientes() {

      var url = soapFactory.url + "/CLIENTEBUSCAR.php";
      var soapAction = soapFactory.soapAction + "/CLIENTEBUSCAR";

      if ( $scope.data.CLIENTEPESQ !== "" ) {

        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );

        $http.post( url, {
            "IDEMPRESA": $scope.data.IDEMPRESA,
            "IDCONEXAO": $scope.data.IDCONEXAO,
            "VENDCOD": $scope.data.VENDCOD,
            "CLIENTEPESQ": $scope.data.CLIENTEPESQ
          }, {
            'Content-Type': soapFactory.contentTypeForm,
            "SOAPAction": soapAction
          } )
          .success( function ( data ) {
            for ( var i = 0; i < data.CLIENTEBUSCARRESULT.STRING.length; i++ ) {
              $scope.separado[ i ] = data.CLIENTEBUSCARRESULT.STRING[ i ].split( "," );
              var n = i - 1;
              $scope.separadoItem[ n ] = {
                idCliente: $scope.separado[ i ][ 0 ],
                nome: $scope.separado[ i ][ 1 ],
                nomeCliente: $scope.separado[ i ][ 1 ],
                fantasia: $scope.separado[ i ][ 2 ],
                uf: $scope.separado[ i ][ 4 ],
                cnpj: $scope.separado[ i ][ 5 ],
                vendedor: $scope.separado[ i ][ 6 ],
                cotacaoEmAberto: $scope.separado[ i ][ 7 ],
              };
            }
            $ionicLoading.hide();

            $scope.data.CLIENTEPESQ = "";

            if ( $scope.separado[ 0 ] == "S" ) {
              
              //Go
            } else {
              $scope.showAlert( "Smartsales", $scope.separado[ 1 ] );
              $ionicLoading.hide();
              // $state.go("login");
            }
          } )
          .error( function ( data ) {
            console.log( data );
            $ionicLoading.hide();
          } );
        $scope.data.CLIENTEPESQ = "";

      } else {
        $scope.showAlert( "Preste Atenção", "Por favor, insira dados no campo" );
      }

      $scope.data.CLIENTEPESQ = "";
      $scope.separado = [];

    } //End listarClientes();

    /*
     * Função que lista as informações do cliente escolhido
     */
    function clienteEspecifico( idCliente ) {

      $scope.cliente = loginFactory.cliente;
      console.log( loginFactory.cliente );

      cotacaoFactory.idCliente = idCliente;

      var url = soapFactory.url + "/CLIENTEDADOS.php";
      var soapAction = soapFactory.soapAction + "/CLIENTEDADOS";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCLIENTE": idCliente
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.CLIENTEDADOSRESULT.STRING.length; i++ ) {
            $rootScope.clienteDados[ i ] = data.CLIENTEDADOSRESULT.STRING[ i ].split( "," );
            console.log( "clientedados" + [ i ] + " = " + $rootScope.clienteDados[ i ] );
            $rootScope.clienteDados[ i ] = $rootScope.clienteDados[ i ].toString();
          }
          $ionicLoading.hide();
          $scope.data.clienteId = $stateParams.clienteId;
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End clienteEspecifico();

    /*
     * Função que atualiza as informações do cliente escolhido
     */
    function clienteAtualizaCadastro() {

      //  $scope.cliente = loginFactory.cliente;
      //  console.log(loginFactory.cliente);
      var url = soapFactory.url + "/CLIENTEATUALIZACADASTRO.php";
      var soapAction = soapFactory.soapAction + "/CLIENTEATUALIZACADASTRO";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCLIENTE": $scope.data.clienteId,
          "NEWRAZAOSOCIAL": $scope.data.NEWRAZAOSOCIAL || $rootScope.clienteDados[ 2 ],
          "NEWTELEFONE": $scope.data.NEWTELEFONE || $rootScope.clienteDados[ 10 ],
          "NEWTELEFONE2": $scope.data.NEWTELEFONE2,
          "NEWEMAIL": $scope.data.NEWEMAIL || $rootScope.clienteDados[ 11 ],
          "NEWCONTATONOME": $scope.data.NEWCONTATONOME || $rootScope.clienteDados[ 3 ],
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.CLIENTEATUALIZACADASTRORESULT.STRING.length; i++ ) {
            $scope.clienteAtualizaDados[ i ] = data.CLIENTEATUALIZACADASTRORESULT.STRING[ i ].split( "," );
            console.log( "clienteAtualizaDados" + [ i ] + " = " + $scope.clienteAtualizaDados[ i ] );
            //  $scope.clienteAtualizaDados[i] = $scope.clienteAtualizaDados[i].toString();
          }
          $ionicLoading.hide();
          if ( $scope.clienteAtualizaDados[ 0 ] == "S" ) {
            $cordovaToast.showShortBottom( $scope.clienteAtualizaDados[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            //  $scope.showAlert("Smartsales", $scope.clienteAtualizaDados[1]);
            history.back();
          } else {
            $cordovaToast.showShortBottom( $scope.clienteAtualizaDados[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            //  $sc
            //  $scope.showAlert("Smartsales", $scope.clienteAtualizaDados[1]);
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );
    } //End clienteAtualizaCadastro();

    /*
     * Função que lista todas as cotações do cliente
     */
    function cotacaoEmAberto( idCliente, clienteNome ) {


      var url = soapFactory.url + "/COTACAOEMABERTO.php";
      var soapAction = soapFactory.soapAction + "/COTACAOEMABERTO";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCLIENTE": idCliente
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          $scope.data.clienteNome = clienteNome;
          cotacaoFactory.nomeCliente = clienteNome;
          cotacaoFactory.idCliente = idCliente;

          console.log( clienteNome );
          console.log( data );
          if ( pageLoad ) {
            pageLoad = false;
            cotacaoTabelaPrecos();
            cotacaoCondPagamento();
            cotacaoTransportadoraSemId();
            parametrosLista();
          }
          for ( var i = 0; i < data.COTACAOEMABERTORESULT.STRING.length; i++ ) {
            $rootScope.cotacaoEmAberto[ i ] = data.COTACAOEMABERTORESULT.STRING[ i ].split( "," );
            console.log( "cotacaoEmAberto" + [ i ] + " = " + $rootScope.cotacaoEmAberto[ i ] );
            //   $rootScope.cotacaoEmAberto[i] = $rootScope.cotacaoEmAberto[i].toString();
            var n = i - 1;
            $rootScope.cotacaoEmAbertoEspecifico[ n ] = {
              idCotacao: $rootScope.cotacaoEmAberto[ i ][ 0 ],
              emissao: $rootScope.cotacaoEmAberto[ i ][ 1 ],
              idCliente: $rootScope.cotacaoEmAberto[ i ][ 2 ],
              fantasia: $rootScope.cotacaoEmAberto[ i ][ 3 ],
              totalGeral: $rootScope.cotacaoEmAberto[ i ][ 4 ],
              qtdItens: $rootScope.cotacaoEmAberto[ i ][ 5 ]
            };
            // console.log("Specific: " + $rootScope.cotacaoEmAberto[i][0]);
          }
          $ionicLoading.hide();

          if ( $rootScope.cotacaoEmAberto[ 0 ] == "S" ) {

          } else {
            $rootScope.cotacaoEmAbertoEspecifico = [];
            $scope.showAlert( "Smartsales", $rootScope.cotacaoEmAberto[ 1 ], "2000" );
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End cotacaoEmAberto();

    /*
     * Função que lista todas as cotações do cliente baseados no parâmetro com id do cliente
     */
    function cotacaoEmAbertoParam() {

      var url = soapFactory.url + "/COTACAOEMABERTO.php";
      var soapAction = soapFactory.soapAction + "/COTACAOEMABERTO";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCLIENTE": $stateParams.clienteId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.COTACAOEMABERTORESULT.STRING.length; i++ ) {
            $rootScope.cotacaoEmAberto[ i ] = data.COTACAOEMABERTORESULT.STRING[ i ].split( "," );
            console.log( "cotacaoEmAberto" + [ i ] + " = " + $rootScope.cotacaoEmAberto[ i ] );
            //   $rootScope.cotacaoEmAberto[i] = $rootScope.cotacaoEmAberto[i].toString();
            var n = i - 1;
            $rootScope.cotacaoEmAbertoEspecifico[ n ] = {
              idCotacao: $rootScope.cotacaoEmAberto[ i ][ 0 ],
              emissao: $rootScope.cotacaoEmAberto[ i ][ 1 ],
              idCliente: $rootScope.cotacaoEmAberto[ i ][ 2 ],
              fantasia: $rootScope.cotacaoEmAberto[ i ][ 3 ],
              totalGeral: $rootScope.cotacaoEmAberto[ i ][ 4 ],
              qtdItens: $rootScope.cotacaoEmAberto[ i ][ 5 ]
            };
            console.log( "Specific: " + $rootScope.cotacaoEmAberto[ i ][ 0 ] );
          }
          $ionicLoading.hide();

          if ( $rootScope.cotacaoEmAberto[ 0 ] == "S" ) {

          } else {
            $rootScope.cotacaoEmAbertoEspecifico = [];
            $scope.showAlert( "Smartsales", $rootScope.cotacaoEmAberto[ 1 ], "2000" );
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End cotacaoEmAbertoParam();

    /*
     * Função que inclui uma cotação para um cliente
     */
    function cotacaoIncluir() {

      function GetData( data ) {
        var deferred = $q.defer();
        console.log( "Passing through" );
        deferred.resolve( data );
        return deferred.promise;
      }
      var dataPromise = new GetData();

      var url = soapFactory.url + "/COTACAOINCLUIR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOINCLUIR";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCLIENTE": $scope.data.clienteId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          //   this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOINCLUIRRESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOINCLUIRRESULT.STRING[ i ].split( "," );
            console.log( "Cotaçäo Default: " + $scope.separado[ i ] );
          }

          // Começa a sequencia syncronizado da promise
          dataPromise
            .then( function () {
              // Grava na Factory Cotacao todos os dados pré-definidos no array que vem do webservice
              cotacaoFactory.idCotacao = $scope.separado[ 1 ].toString();
              console.log( "ID da cotacao: " + cotacaoFactory.idCotacao );
              cotacaoFactory.desconto1 = $scope.separado[ 2 ].toString();
              cotacaoFactory.desconto2 = $scope.separado[ 3 ].toString();
              cotacaoFactory.desconto3 = $scope.separado[ 4 ].toString();
              cotacaoFactory.desconto4 = $scope.separado[ 5 ].toString();
              //Zerando os valores padrões dos selects
              cotacaoFactory.tabelaPreco = {};
              cotacaoFactory.condPgmt = {};
              cotacaoFactory.transportadora = {};
              cotacaoFactory.tipoFrete = {};

              //Alimentando os selects com os valores padrões
              cotacaoFactory.tabelaPreco.id = $scope.separado[ 8 ].toString();
              cotacaoFactory.tabelaPreco.nome = $scope.separado[ 9 ].toString();
              //Alimentando os selects com os valores padrões
              cotacaoFactory.condPgmt.id = $scope.separado[ 12 ].toString();
              cotacaoFactory.condPgmt.nome = $scope.separado[ 13 ].toString();
              //Alimentando os selects com os valores padrões
              cotacaoFactory.transportadora.id = $scope.separado[ 6 ].toString();
              cotacaoFactory.transportadora.nome = $scope.separado[ 7 ].toString();
              //Alimentando os selects com os valores padrões
              cotacaoFactory.tipoFrete.id = $scope.separado[ 10 ].toString();
              cotacaoFactory.tipoFrete.nome = $scope.separado[ 11 ].toString();


            } );
          dataPromise //Chamando as funções para listar os selects
            .then( function () {
            cotacaoTabelaPrecos();
            return $rootScope.condTabelaPrecosSeparado;
          } );
          dataPromise
            .then( function () {
              cotacaoCondPagamento();
              return $rootScope.condPgmtSeparado;
            } );
          dataPromise
            .then( function () {
              cotacaoTransportadora();
              return $rootScope.transSeparado;
            } );
          dataPromise
            .then( function () {
              parametrosLista();
              return $rootScope.paraListaSeparado;
            } );
          dataPromise
          //     .then(function() {
          //         //Coloca um default no select do cliente tabela de preços
          //         for (var lilTabela in $rootScope.condTabelaPrecosSeparado) {
          //             if ($rootScope.condTabelaPrecosSeparado[lilTabela].id) {
          //                 if ($scope.separado[3].toString() == $rootScope.condTabelaPrecosSeparado[lilTabela].id) {
          //                     cotacaoFactory.tabelaPreco = {};
          //                     cotacaoFactory.tabelaPreco.id = $rootScope.condTabelaPrecosSeparado[lilTabela].id;
          //                     cotacaoFactory.tabelaPreco.nome = $rootScope.condTabelaPrecosSeparado[lilTabela].nome;
          //                     console.log("Tabela de preço Default: " + cotacaoFactory.tabelaPreco.nome);
          //                 }
          //             }
          //         }
          //         console.log($scope.separado[3].toString());
          //     }, function(err) {
          //         console.log(err);
          //     })
          //     .then(function() {
          //         //Coloca um default no select do cliente de condição de pagamento
          //         for (var lilPgmt in $rootScope.condPgmtSeparado) {
          //             if ($rootScope.condPgmtSeparado[lilPgmt].id) {
          //                 if ($scope.separado[5].toString() == $rootScope.condPgmtSeparado[lilPgmt].id) {
          //                     cotacaoFactory.condPgmt = {};
          //                     cotacaoFactory.condPgmt.id = $rootScope.condPgmtSeparado[lilPgmt].id;
          //                     cotacaoFactory.condPgmt.nome = $rootScope.condPgmtSeparado[lilPgmt].nome;
          //                     console.log("Condicao de pagamento Default: " + cotacaoFactory.condPgmt.nome);
          //                 }
          //             }
          //         }
          //         console.log($scope.separado[5].toString());
          //     }, function(err) {
          //         console.log(err);
          //     })
          //     .then(function() {
          //         //Coloca um default no select do cliente de transportadoras
          //         for (var lilParam in $rootScope.transSeparado) {
          //             if ($rootScope.transSeparado[lilParam].id) {
          //                 if ($scope.separado[2].toString() == $rootScope.transSeparado[lilParam].id) {
          //                     cotacaoFactory.transportadora = {};
          //                     cotacaoFactory.transportadora.id = $rootScope.transSeparado[lilParam].id;
          //                     cotacaoFactory.transportadora.nome = $rootScope.transSeparado[lilParam].nome;
          //                     console.log("Transportadora Default: " + cotacaoFactory.transportadora.nome);
          //                 }
          //             }
          //         }
          //         console.log($scope.separado[2].toString());
          //     }, function(err) {
          //         console.log(err);
          //     })
          //     .then(function() {
          //         //Coloca um default no select do cliente de tipos de fete
          //         for (var lilFrete in $rootScope.paraListaSeparado) {
          //             if ($rootScope.paraListaSeparado[lilFrete].id) {
          //                 if ($scope.separado[4].toString() == $rootScope.paraListaSeparado[lilFrete].id) {
          //                     cotacaoFactory.tipoFrete = {};
          //                     cotacaoFactory.tipoFrete.id = $scope.separado[4].toString();
          //                     cotacaoFactory.tipoFrete.nome = $rootScope.paraListaSeparado[lilFrete].nome;
          //                     console.log("Tipo de frete Default: " + cotacaoFactory.tipoFrete.nome);
          //                 }
          //             }
          //         }
          //         console.log($scope.separado[4].toString());
          //     }, function(err) {
          //         console.log(err);
          //     })
            .then( function () {
              $ionicLoading.hide();
            } )
            .finally( function () {

              if ( $scope.separado[ 0 ] == "S" ) {
                $cordovaToast.showShortBottom( 'Cotação ' + $scope.separado[ 1 ] + " criado" )
                  .then( function ( success ) {
                    console.log( "Toast success" );
                  }, function ( error ) {
                    console.log( "Toast failed" );
                  } );
                $state.go( "addCotacao", {
                  clienteId: $scope.data.clienteId
                } );
                // cotacaoEmAbertoParam();
              } else {
                $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
              }

            } ); //End promise

            $state.go( "addCotacao");

        } )
        .error(
          function ( data ) {
            console.log( data );
          }
        );

    } //End cotacaoIncluir

    /*
     * Função que lista os dados da cotação
     */
    function cotacaoDadosListar( idCotacao ) {

      function GetData( data ) {
        var deferred = $q.defer();
        console.log( "Passing through" );
        deferred.resolve( data );
        return deferred.promise;
      }
      var dataPromise = new GetData();

      var url = soapFactory.url + "/COTACAODADOS.php";
      var soapAction = soapFactory.soapAction + "/COTACAODADOS";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": idCotacao
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          for ( var i = 0; i < data.COTACAODADOSRESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAODADOSRESULT.STRING[ i ].split( "," );
            console.log( "CotacaoDadosListar(): " + $scope.separado[ i ] );
          } // End For

          // Começa a sequencia syncronizado da promise
          dataPromise
            .then( function () {
              // Grava na Factory Cotacao todos os dados pré-definidos no array que vem do webservice
              cotacaoFactory.idCotacao = $scope.separado[ 1 ].toString();
              console.log( "ID da cotacao: " + cotacaoFactory.idCotacao );
              cotacaoFactory.desconto1 = $scope.separado[ 7 ].toString();
              cotacaoFactory.desconto2 = $scope.separado[ 8 ].toString();
              cotacaoFactory.desconto3 = $scope.separado[ 9 ].toString();
              cotacaoFactory.desconto4 = $scope.separado[ 10 ].toString();

              cotacaoTabelaPrecos();
              return $rootScope.condTabelaPrecosSeparado;
            } );
          dataPromise
            .then( function () {
              cotacaoCondPagamento();
              return $rootScope.condPgmtSeparado;
            } );
          dataPromise
            .then( function () {
              cotacaoTransportadora();
              return $rootScope.transSeparado;
            } );
          dataPromise
            .then( function () {
              parametrosLista();
              return $rootScope.paraListaSeparado;
            } );
          dataPromise
            .then( function () {
              //Coloca um default no select do cliente tabela de preços
              for ( var lilTabela in $rootScope.condTabelaPrecosSeparado ) {
                if ( $rootScope.condTabelaPrecosSeparado[ lilTabela ].id ) {
                  if ( $scope.separado[ 6 ].toString() == $rootScope.condTabelaPrecosSeparado[ lilTabela ].id ) {
                    cotacaoFactory.tabelaPreco = {};
                    cotacaoFactory.tabelaPreco.id = $rootScope.condTabelaPrecosSeparado[ lilTabela ].id;
                    cotacaoFactory.tabelaPreco.nome = $rootScope.condTabelaPrecosSeparado[ lilTabela ].nome;
                    console.log( "Tabela de preço Default: " + cotacaoFactory.tabelaPreco.nome );
                  }
                }
              }
              console.log( $scope.separado[ 6 ].toString() );
            }, function ( err ) {
              console.log( err );
            } )
            .then( function () {
              //Coloca um default no select do cliente de condição de pagamento
              for ( var lilPgmt in $rootScope.condPgmtSeparado ) {
                if ( $rootScope.condPgmtSeparado[ lilPgmt ].id ) {
                  if ( $scope.separado[ 5 ].toString() == $rootScope.condPgmtSeparado[ lilPgmt ].id ) {
                    cotacaoFactory.condPgmt = {};
                    cotacaoFactory.condPgmt.id = $rootScope.condPgmtSeparado[ lilPgmt ].id;
                    cotacaoFactory.condPgmt.nome = $rootScope.condPgmtSeparado[ lilPgmt ].nome;
                    console.log( "Condicao de pagamento Default: " + cotacaoFactory.condPgmt.nome );
                  }
                }
              }
              console.log( $scope.separado[ 5 ].toString() );
            }, function ( err ) {
              console.log( err );
            } )
            .then( function () {
              //Coloca um default no select do cliente de transportadoras
              for ( var lilParam in $rootScope.transSeparado ) {
                if ( $rootScope.transSeparado[ lilParam ].id ) {
                  if ( $scope.separado[ 14 ].toString() == $rootScope.transSeparado[ lilParam ].id ) {
                    cotacaoFactory.transportadora = {};
                    cotacaoFactory.transportadora.id = $rootScope.transSeparado[ lilParam ].id;
                    cotacaoFactory.transportadora.nome = $rootScope.transSeparado[ lilParam ].nome;
                    console.log( "Transportadora Default: " + cotacaoFactory.transportadora.nome );
                  }
                }
              }
              console.log( $scope.separado[ 14 ].toString() );
            }, function ( err ) {
              console.log( err );
            } )
            .then( function () {
              //Coloca um default no select do cliente de tipos de fete
              for ( var lilFrete in $rootScope.paraListaSeparado ) {
                if ( $rootScope.paraListaSeparado[ lilFrete ].id ) {
                  if ( $scope.separado[ 15 ].toString() == $rootScope.paraListaSeparado[ lilFrete ].id ) {
                    cotacaoFactory.tipoFrete = {};
                    cotacaoFactory.tipoFrete.id = $scope.separado[ 15 ].toString();
                    cotacaoFactory.tipoFrete.nome = $rootScope.paraListaSeparado[ lilFrete ].nome;
                    console.log( "Tipo de frete Default: " + cotacaoFactory.tipoFrete.nome );
                  }
                }
              }
              console.log( $scope.separado[ 15 ].toString() );
            }, function ( err ) {
              console.log( err );
            } )
            .then( function () {
              $ionicLoading.hide();
            } )
            .finally( function () {
              $state.go( "alterarCotacao", {
                clienteId: $scope.data.clienteId,
                cotacaoId: idCotacao
              } );
            } );

        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );
      //   $scope.separado = [];

    } // end cotacaoDadosListar

    /*
     * Função que lista os dados da cotação
     */
    function cotacaoDadosDescontos() {

      var url = soapFactory.url + "/COTACAODADOS.php";
      var soapAction = soapFactory.soapAction + "/COTACAODADOS";


      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": $stateParams.cotacaoId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          for ( var i = 0; i < data.COTACAODADOSRESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAODADOSRESULT.STRING[ i ].split( "," );
            console.log( "CotacaoDadosDescontos: " + $scope.separado[ i ] );
          }
          // Grava na Factory Cotacao todos os dados pré-definidos no array que vem do webservice
          cotacaoFactory.desconto1 = $scope.separado[ 7 ].toString();
          cotacaoFactory.desconto2 = $scope.separado[ 8 ].toString();
          cotacaoFactory.desconto3 = $scope.separado[ 9 ].toString();
          cotacaoFactory.desconto4 = $scope.separado[ 10 ].toString();

          $ionicLoading.hide();
        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } // end cotacaoDadosDescontos

    /*
     * Função que permite a alteração da cotação de um cliente
     */
    function alterarCotacao() {
      console.log( "idcotacao =  " + loginFactory.cotacao );

      var url = soapFactory.url + "/COTACAOATUALIZA.php";
      var soapAction = soapFactory.soapAction + "/COTACAOATUALIZA";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": cotacaoFactory.idCotacao || $scope.data.cotacaoId,
          "IDCLIENTE": $scope.data.clienteId,
          "IDCONDICAO": $rootScope.cotacaoDados.condPgmt.id,
          "IDTABELAPRECO": $rootScope.cotacaoDados.tabelaPreco.id,
          "IDTRANSPORTADORA": $rootScope.cotacaoDados.transportadora.id,
          "TIPOFRETE": $rootScope.cotacaoDados.tipoFrete.id,
          "DESCONTO1": $rootScope.cotacaoDados.desconto1,
          "DESCONTO2": $rootScope.cotacaoDados.desconto2,
          "DESCONTO3": $rootScope.cotacaoDados.desconto3,
          "DESCONTO4": $rootScope.cotacaoDados.desconto4,
          "OBSERVACOES": $rootScope.cotacaoDados.observacoes
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          for ( var i = 0; i < data.COTACAOATUALIZARESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOATUALIZARESULT.STRING[ i ].split( "," );
            console.log( "Dados cotação: " + $scope.separado[ i ] );
          }
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            $scope.showAlert( "Smartsales", $scope.separado[ 1 ] );
          } else {
            $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
          }
        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );
      $scope.separado = [];
    } //End alterarCotacao();

    /*
     * Função que altera uma cotação de um cliente basesado no parâmetro com o id do cliente
     */
    function alterarCotacaoParam() {
      console.log( "idcotacao =  " + loginFactory.cotacao );

      var url = soapFactory.url + "/COTACAOATUALIZA.php";
      var soapAction = soapFactory.soapAction + "/COTACAOATUALIZA";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": $scope.data.cotacaoId,
          "IDCLIENTE": $scope.data.clienteId,
          "IDCONDICAO": $rootScope.cotacaoDados.condPgmt.id,
          "IDTABELAPRECO": $rootScope.cotacaoDados.tabelaPreco.id,
          "IDTRANSPORTADORA": $rootScope.cotacaoDados.transportadora.id,
          "TIPOFRETE": $rootScope.cotacaoDados.tipoFrete.id,
          "DESCONTO1": $rootScope.cotacaoDados.desconto1,
          "DESCONTO2": $rootScope.cotacaoDados.desconto2,
          "DESCONTO3": $rootScope.cotacaoDados.desconto3,
          "DESCONTO4": $rootScope.cotacaoDados.desconto4,
          "OBSERVACOES": $rootScope.cotacaoDados.observacoes
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          console.log( data );
          for ( var i = 0; i < data.COTACAOATUALIZARESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOATUALIZARESULT.STRING[ i ].split( "," );
            console.log( "Dados cotação: " + $scope.separado[ i ] );
          }
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            $scope.showAlert( "Smartsales", $scope.separado[ 1 ] );
          } else {
            $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
          }
        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );
      //   $scope.separado = [];
    } //End alterarCotacaoParam();

    cotacaoDadosListar


    /*
     * Função que lista as opções de transportadora do cliente
     */
    function cotacaoTransportadora( idCliente ) {

      var url = soapFactory.url + "/COTACAOTRANSPORTADORA.php";
      var soapAction = soapFactory.soapAction + "/COTACAOTRANSPORTADORA";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDTIPO": "CLIENTE",
          "PESQPARAM": $scope.data.clienteId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOTRANSPORTADORARESULT.STRING.length; i++ ) {
            $scope.trans[ i ] = data.COTACAOTRANSPORTADORARESULT.STRING[ i ].split( "," );
            console.log( "Transportadoras: " + $scope.trans[ i ] );
            var n = i - 1;

            $rootScope.transSeparado[ n ] = {
              id: $scope.trans[ i ][ 0 ],
              nome: $scope.trans[ i ][ 1 ]
            };
            //  if ($rootScope.transSeparado[n].id === cotacaoFactory.transportadora.id) {
            //      cotacaoFactory.transportadora.nome = $rootScope.transSeparado.nome;
            //  }

            // console.log("Nome da Transportadora: " + $rootScope.transSeparado[n].nome);

            if ( $scope.trans[ 0 ] == "S" ) {
              // $rootScope.transSeparado[0].nome = $scope.trans[1] + $scope.trans[2];
            } else {
              $rootScope.transSeparado[ -1 ].nome = $scope.trans[ 1 ];
            }

          }

        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } //End cotacaoTransportadora();

    function cotacaoTransportadoraSemId() {

      var url = soapFactory.url + "/COTACAOTRANSPORTADORA.php";
      var soapAction = soapFactory.soapAction + "/COTACAOTRANSPORTADORA";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDTIPO": "CLIENTE",
          "PESQPARAM": cotacaoFactory.idCliente
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOTRANSPORTADORARESULT.STRING.length; i++ ) {
            $scope.trans[ i ] = data.COTACAOTRANSPORTADORARESULT.STRING[ i ].split( "," );
            console.log( "Transportadoras: " + $scope.trans[ i ] );
            var n = i - 1;

            $rootScope.transSeparado[ n ] = {
              id: $scope.trans[ i ][ 0 ],
              nome: $scope.trans[ i ][ 1 ]
            };
            //  if ($rootScope.transSeparado[n].id === cotacaoFactory.transportadora.id) {
            //      cotacaoFactory.transportadora.nome = $rootScope.transSeparado.nome;
            //  }

            // console.log("Nome da Transportadora: " + $rootScope.transSeparado[n].nome);

            if ( $scope.trans[ 0 ] == "S" ) {
              // $rootScope.transSeparado[0].nome = $scope.trans[1] + $scope.trans[2];
            } else {
              $rootScope.transSeparado[ -1 ].nome = $scope.trans[ 1 ];
            }

          }

        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } //End cotacaoTransportadoraSemId();

    /*
     * Função que que lista as opções de condição de pagamento do cliente
     */
    function cotacaoCondPagamento() {

      var url = soapFactory.url + "/COTACAOCONDPAGAMENTO.php";
      var soapAction = soapFactory.soapAction + "/COTACAOCONDPAGAMENTO";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOCONDPAGAMENTORESULT.STRING.length; i++ ) {
            $scope.condPgmt[ i ] = data.COTACAOCONDPAGAMENTORESULT.STRING[ i ].split( "," );
            var n = i - 1;
            $rootScope.condPgmtSeparado[ n ] = {
              id: $scope.condPgmt[ i ][ 0 ],
              nome: $scope.condPgmt[ i ][ 1 ]
            };
            console.log( "Nome da condicao pag: " + $rootScope.condPgmtSeparado[ n ].nome );

          }
          if ( $scope.condPgmt[ 0 ] == "S" ) {
            // $rootScope.transSeparado[0].nome = $scope.trans[1] + $scope.trans[2];
          } else {
            $rootScope.condPgmtSeparado[ -1 ].nome = $scope.condPgmt[ 2 ];
          }

        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } //End cotacaoCondPagamento();

    /*
     * Função que lista as opções de tabela de preço do cliente
     */
    function cotacaoTabelaPrecos() {

      var url = soapFactory.url + "/COTACAOTABELAPRECOS.php";
      var soapAction = soapFactory.soapAction + "/COTACAOTABELAPRECOS";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOTABELAPRECOSRESULT.STRING.length; i++ ) {
            $scope.condTabelaPrecos[ i ] = data.COTACAOTABELAPRECOSRESULT.STRING[ i ].split( "," );
            var n = i - 1;
            $rootScope.condTabelaPrecosSeparado[ n ] = {
              id: $scope.condTabelaPrecos[ i ][ 0 ],
              nome: $scope.condTabelaPrecos[ i ][ 1 ]
            };
            // console.log("Nome da tabela preco: " + $rootScope.condTabelaPrecosSeparado[n].nome);

          }

        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } //End cotacaoTabelaPrecos();

    /*
     * Função que lista os tipo de frete do cliente
     */
    function parametrosLista() {

      var url = soapFactory.url + "/PARAMETROSDADOS.php";
      var soapAction = soapFactory.soapAction + "/PARAMETROSDADOS";
      console.log( $scope.data.IDEMPRESA );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "CPARAMETRO": "05",
          "CFILTRO": ""
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.PARAMETROSDADOSRESULT.STRING.length; i++ ) {
            $scope.paraLista[ i ] = data.PARAMETROSDADOSRESULT.STRING[ i ].split( "," );
            console.log( "fretes: " + $scope.paraLista[ i ] );
            var n = i - 1;
            $rootScope.paraListaSeparado[ n ] = {
              id: $scope.paraLista[ i ][ 0 ],
              nome: $scope.paraLista[ i ][ 1 ]
            };
          }

        } )

      .error( function ( data ) {
        $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        $ionicLoading.hide();
      } );

    } //End parametrosLista();

    /*
     * Função que inclui um item na lista de pedidos de uma cotação de um cliente
     */
    function cotacaoItemIncluir() {

      var url = soapFactory.url + "/COTACAOITEMINCLUIR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOITEMINCLUIR";


      console.log( "precoUnitario: " + $scope.cotacaoDados.produtoPrecoUnit.valor );
      console.log( "quantidadeProduto: " + $scope.cotacaoDados.produtoQuantCot );
      console.log( "Desconto: " + $scope.cotacaoDados.produtoDescontoPerc );


      if ( $scope.cotacaoDados.produtoQuantCot <= $scope.cotacaoDados.produtoQuantDisp ) {

        $http.post( url, {
            "IDEMPRESA": $scope.data.IDEMPRESA,
            "IDCOTACAO": $scope.data.cotacaoId,
            "IDCONEXAO": $scope.data.IDCONEXAO,
            "IDPRODUTO": $rootScope.cotacaoDados.produtoId,
            "PRECOUNITARIO": $rootScope.cotacaoDados.produtoPrecoUnit.valor,
            "QUANTIDADE": $rootScope.cotacaoDados.produtoQuantCot,
            "DESCONTOPERC": $rootScope.cotacaoDados.produtoDescontoPerc,
          }, {
            'Content-Type': soapFactory.contentTypeForm,
            "SOAPAction": soapAction
          } )
          .success( function ( data ) {
            // this callback will be called asynchronously
            for ( var i = 0; i < data.COTACAOITEMINCLUIRRESULT.STRING.length; i++ ) {
              $scope.cotItemIncluir[ i ] = data.COTACAOITEMINCLUIRRESULT.STRING[ i ].split( "," );
              console.log( $scope.cotItemIncluir[ i ] );

            }
            if ( $scope.cotItemIncluir[ 0 ] == "S" ) {
              showAlert( $scope.cotItemIncluir[ 1 ], "Total da Cotação: " + parseFloat( $scope.cotItemIncluir[ 2 ] ).toFixed( 2 ) + "<br> Total de Itens na Cotação: " + $scope.cotItemIncluir[ 3 ] );
              $state.go( "inserirItensCotacao", {
                clienteId: $stateParams.clienteId,
                cotacaoId: $stateParams.cotacaoId
              } );

              $rootScope.cotacaoDados.produtoPrecoUnit = "";
              $rootScope.cotacaoDados.produtoQuantCot = "";
              $rootScope.cotacaoDados.produtoDescontoPerc = "";
              cotacaoFactory.produtoValorTotal = "";
            } else {
              showAlert( "Smartsales", $scope.cotItemIncluir[ 1 ] );
            }

          } )

        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

      } else {
        showAlert( "Opppss!", "Ocorreu um erro na inclusão de item! <br>Por favor veirfique e tente novamente!" );
      }



    } //End cotacaoItemIncluir();

    /*
     * Função que transforma uma cotação em pedido de venda
     */
    function cotacaoPedidoGravar( idCotacao ) {

      var url = soapFactory.url + "/COTACAOPEDIDOGRAVAR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOPEDIDOGRAVAR";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCOTACAO": idCotacao,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOPEDIDOGRAVARRESULT.STRING.length; i++ ) {
            $scope.cotPedidoGravar[ i ] = data.COTACAOPEDIDOGRAVARRESULT.STRING[ i ].split( "," );
            console.log( $scope.cotPedidoGravar[ i ] );

          }
          if ( $scope.cotPedidoGravar[ 0 ] == "S" ) {
            $cordovaToast.showLongBottom( $scope.cotPedidoGravar[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // showAlert("Smartsales", $scope.cotPedidoGravar[1]);
            $state.go( "menuClientes", {
              clienteId: $scope.data.clienteId
            } );
            cotacaoEmAbertoParam();
          } else {
            $cordovaToast.showLongBottom( $scope.cotPedidoGravar[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // showAlert("Smartsales", $scope.cotPedidoGravar[1]);
          }

        } )

      .error( function ( data ) {
        $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        $ionicLoading.hide();
      } );

    } // End cotacaoPedidoGravar();

    function cotacaoPedidoGravarResumo() {

      var url = soapFactory.url + "/COTACAOPEDIDOGRAVAR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOPEDIDOGRAVAR";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCOTACAO": $stateParams.cotacaoId,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOPEDIDOGRAVARRESULT.STRING.length; i++ ) {
            $scope.cotPedidoGravar[ i ] = data.COTACAOPEDIDOGRAVARRESULT.STRING[ i ].split( "," );
            console.log( $scope.cotPedidoGravar[ i ] );
          }
          if ( $scope.cotPedidoGravar[ 0 ] == "S" ) {
            $cordovaToast.showLongBottom( $scope.cotPedidoGravar[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // showAlert("Smartsales", $scope.cotPedidoGravar[1]);
            $state.go( "menuClientes", {
              clienteId: $scope.data.clienteId
            } );
            cotacaoEmAbertoParam();
          } else {
            $cordovaToast.showLongBottom( $scope.cotPedidoGravar[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // showAlert("Smartsales", $scope.cotPedidoGravar[1]);
          }

        } )

      .error( function ( data ) {
        $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        $ionicLoading.hide();
      } );

    } // End cotacaoPedidoGravarResumo();

    /*
     * Função que lista todos os pedidos de venda do cliente
     */
    function clientePedidosNfs() {

      var url = soapFactory.url + "/CLIENTEPEDIDOSNFS.php";
      var soapAction = soapFactory.soapAction + "/CLIENTEPEDIDOSNFS";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCLIENTE": $scope.data.clienteId,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.CLIENTEPEDIDOSNFSRESULT.STRING.length; i++ ) {
            $scope.cliPedidosNfs[ i ] = data.CLIENTEPEDIDOSNFSRESULT.STRING[ i ].split( "," );
            console.log( "Pedidos" + [ i ] + ": " + $scope.cliPedidosNfs[ i ] );
            var n = i - 1;
            $rootScope.cliPedidosNfsSeparado[ n ] = {
              //  blan0 : $scope.cliPedidosNfs[i][0],
              id: $scope.cliPedidosNfs[ i ][ 1 ],
              //  blank2 : $scope.cliPedidosNfs[i][2],
              //  nome : $scope.cliPedidosNfs[i][3],
              //  nome : $scope.cliPedidosNfs[i][4],
              valor: $scope.cliPedidosNfs[ i ][ 5 ],
              //  nome : $scope.cliPedidosNfs[i][6],
              //  nome : $scope.cliPedidosNfs[i][7],
              pdf: $scope.cliPedidosNfs[ i ][ 8 ]
            };
            console.log( "Pedidos" + [ i ] + ": " + $scope.cliPedidosNfs[ i ] );
          }
          console.log( "Pedido" + [ 0 ] + ": " + $scope.cliPedidosNfs[ i ] );
          if ( $scope.cliPedidosNfs[ 0 ] == "S" ) {
            //
          } else {
            $cordovaToast.showLongBottom( $scope.cliPedidosNfs[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // showAlert("Smartsales", $scope.cliPedidosNfs[1]);
          }

        } )

      .error( function ( data ) {
        $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        $ionicLoading.hide();
      } );

    } // End clientePedidosNfs();

    /*
     * Função que lista as posições financeiras de um cliente
     */
    function clienteFinanceiro() {

      // Funcao para buscar outros tipos de transportadora
      var url = soapFactory.url + "/CLIENTEFINANCEIRO.php";
      var soapAction = soapFactory.soapAction + "/CLIENTEFINANCEIRO";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "1000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCLIENTE": $scope.data.clienteId,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.CLIENTEFINANCEIRORESULT.STRING.length; i++ ) {
            $rootScope.cliFinanceiro[ i ] = data.CLIENTEFINANCEIRORESULT.STRING[ i ].split( "," );
            console.log( "financeiro" + $rootScope.cliFinanceiro[ i ] );
            var n = i - 1;
            $rootScope.cliFinanceiroSeparado[ n ] = {
              prefixo: $scope.cliFinanceiro[ i ][ 0 ],
              numero: $rootScope.cliFinanceiro[ i ][ 1 ],
              parcela: $scope.cliFinanceiro[ i ][ 2 ],
              tipo: $scope.cliFinanceiro[ i ][ 3 ],
              valor: $scope.cliFinanceiro[ i ][ 4 ],
              observacao: $rootScope.cliFinanceiro[ i ][ 5 ],
              emissao: $scope.cliFinanceiro[ i ][ 6 ],
              vencimento: $scope.cliFinanceiro[ i ][ 7 ],
              baixa: $rootScope.cliFinanceiro[ i ][ 8 ],
            };
          }

          if ( $rootScope.cliFinanceiro[ 0 ] == "S" ) {
            //
          } else {
            $cordovaToast.showLongBottom( $rootScope.cliFinanceiro[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // showAlert("Smartsales", $rootScope.cliFinanceiro[1]);
          }
        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } //End clienteFinanceiro

    /*
     * Função que faz a exclusão de um item de uma cotação
     */
    function cotacaoItemExcluir( produtoId ) {

      var url = soapFactory.url + "/COTACAOITEMEXCLUIR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOITEMEXCLUIR";

      $http.post( url, {
          "IDEMPRESA": loginFactory.idEmpresa,
          "IDCONEXAO": loginFactory.idconexao,
          "IDCOTACAO": $stateParams.cotacaoId,
          "IDITEM": produtoId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          for ( var i = 0; i < data.COTACAOITEMEXCLUIRRESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOITEMEXCLUIRRESULT.STRING[ i ].split( "," );
            console.log( "cotacaoItemExcluir" + $scope.separado[ i ] );
          }
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            $cordovaToast.showLongBottom( "Item excluida com sucesso!" );
          } else {
            $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
          }
        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        } );
    } //End cotacaoItemExcluir();

    function cotacaoitemalterar( idItem ) {

      var url = soapFactory.url + "/COTACAOITEMALTERAR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOITEMALTERAR";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "IDCOTACAO": $scope.data.cotacaoId,
          "IDPRODUTO": cotacaoFactory.produtoId,
          "PRECOUNITARIO": $rootScope.cotacaoDados.produtoPrecoUnit.valor,
          "QUANTIDADE": $rootScope.cotacaoDados.produtoQuantCot,
          "DESCONTOPERC": $scope.cotacaoDados.produtoDescontoPerc,
          "IDITEM": cotacaoFactory.itemId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          for ( var i = 0; i < data.COTACAOITEMALTERARRESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOITEMALTERARRESULT.STRING[ i ].split( "," );
            console.log( "COTACAOITEMALTERAR" + $scope.separado[ i ] );
          }
          console.log( "Id do item: " + cotacaoFactory.itemId );
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            $cordovaToast.showLongBottom( "Item alterado com sucesso!" );
          } else {
            $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
          }
        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        } );
    } //End cotacaoItemAlterar();

    /*
     * Função que lista os dados do vendedor (AINDA NÃO FOI IMPLEMENTADO NO WEB SERVICE -> 24/09/15);
     */
    function cotacaoVendAberto() {
      var url = soapFactory.url + "/COTACAOVENDABERTO.php";
      var soapAction = soapFactory.soapAction + "/COTACAOVENDABERTO";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "VENDCOD": loginFactory.vendcod
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          for ( var i = 0; i < data.COTACAOVENDABERTORESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOVENDABERTORESULT.STRING[ i ].split( "," );
            console.log( "COTACAOVENDABERTO" + $scope.separado[ i ] );
          }
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            //
          } else {
            $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
          }
        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        } );
    } // End cotacaoVendAberto();


  } //End controller

} )();
