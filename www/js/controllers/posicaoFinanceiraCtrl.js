( function () {
  'use strict';

  angular.module( 'smartsales.controllers.posicaoFinanceiraCtrl', [] )

  /* @Controller*/
  .controller( "posicaoFinanceiraCtrl", posicaoFinanceiraCtrl );

  /* Injeção de dependência*/
  posicaoFinanceiraCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading", "$state", "$ionicActionSheet", "$q", "$timeout",
    "$routeParams", "$stateParams", "$cordovaToast", "$localstorage"
  ];

  /*Declara uma função para ser usado no controller 'posicaoFinanceiraCtrl' e com todas as funções do $scope Cliente*/
  function posicaoFinanceiraCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicActionSheet, $q, $timeout, $routeParams, $stateParams, $cordovaToast, $localstorage) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Posição Financeira');

      $scope.showAlert = showAlert;
      $scope.cliFinanceiroSeparado = [];

      //Executa a funcao clienteFinanceiro
      clienteFinanceiro();


    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    /*Função que lista as posições financeiras de um cliente*/
    function clienteFinanceiro() {


      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&IDCLIENTE="+ $localstorage.get('idCliente');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/CLIENTEFINANCEIRO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.CLIENTEFINANCEIRORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.CLIENTEFINANCEIRORESULT.STRING.length; i++ ) {

              var n = i -1;

              $scope.cliFinanceiroSeparado[n] = {
                prefixo: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[0],
                numero: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[1],
                parcela: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[2],
                tipo: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[3],
                valor: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[4],
                observacao: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[5],
                emissao: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[6],
                vencimento: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[7],
                baixa: data.CLIENTEFINANCEIRORESULT.STRING[i].split(",")[8]
              };
            }


            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.CLIENTEFINANCEIRORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End clienteFinanceiro

}} )();