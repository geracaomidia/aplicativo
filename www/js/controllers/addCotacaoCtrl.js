( function () {
  'use strict';

  angular.module( 'smartsales.controllers.addCotacaoCtrl', [ 'ngCordova', "ui.router" ] )

  /* @Controller*/
  .controller( "addCotacaoCtrl", addCotacaoCtrl );

  /* Injeção de dependência*/
  addCotacaoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$cordovaToast", "$ionicPlatform", "$stateParams", "$ionicActionSheet", "$localstorage"
  ];

  /*  Declara uma função para ser usado no controller 'produtoCtrl' e com todas as funções do $scope Cliente*/
  function addCotacaoCtrl($ionicPopup, $rootScope, $scope, $state, $http, $ionicLoading, $cordovaToast, $ionicPlatform, $stateParams, $ionicActionSheet, $localstorage) {

    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Nova Cotação');

      $scope.showAlert = showAlert;
      $rootScope.condTabelaPrecosSeparado = [];
      $rootScope.condPgmtSeparado = [];
      $rootScope.transSeparado = [];
      $rootScope.paraListaSeparado = [];

      $rootScope.cotacao = {
        id: $localstorage.get("IDCOTACAO"),
        desconto1: $localstorage.get("DESCONTO1"),
        desconto2: $localstorage.get("DESCONTO2"),
        desconto3: $localstorage.get("DESCONTO3"),
        desconto4: $localstorage.get("DESCONTO4"),
        transpID: $localstorage.get("TRANSPID"),
        transpDESC: $localstorage.get("TRANSPDESC"),
        tabelaID: $localstorage.get("TABELAID"),
        tabelaDESC: $localstorage.get("TABELADESC"),
        freteID: $localstorage.get("FRETEID"),
        freteDESC: $localstorage.get("FRETEDESC"),
        condPAGTO: $localstorage.get("CONDPAGTO"),
        condPAGTOdesc: "",
        observacoes: ""
      };

      //Chama funcao de cotacaoDados para pegar informações do cliente da cotação
      cotacaoDados();

      // //Chama funcao para carregar opções de tabela de preços
      // cotacaoTabelaPrecos();

      // //Chama função para carregar condições de pagamento
      // cotacaoCondPagamento();

      // //Chama função para carregar transportadoras
      // cotacaoTransportadora();

      // //Chama função para carregar parametros
      // parametrosLista();

    }//End onDeviceReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Função cotacaoDados
    function cotacaoDados(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Cotação....',
        duration: "60000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAODADOS.php", vars)
        .done(function(data){

          //alert(data.COTACAODADOSRESULT.STRING);

          //Recebe o status de retorno
          $scope.status = data.COTACAODADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            $rootScope.cotacaoCliente = {
              id: data.COTACAODADOSRESULT.STRING[3],
              nome: data.COTACAODADOSRESULT.STRING[4]
            };


          $rootScope.cotacao.transpID = data.COTACAODADOSRESULT.STRING[16];

          $rootScope.cotacao.tabelaID = data.COTACAODADOSRESULT.STRING[7];

          $rootScope.cotacao.freteID = data.COTACAODADOSRESULT.STRING[18];

          $rootScope.cotacao.condPAGTO = data.COTACAODADOSRESULT.STRING[5];

          $rootScope.cotacao.observacoes = data.COTACAODADOSRESULT.STRING[15];


            //Salva em localstorage informações do cliente da cotação
            $localstorage.set("IDCLIENTECOTACAO", data.COTACAODADOSRESULT.STRING[3]);
            $localstorage.set("NOMECLIENTECOTACAO", data.COTACAODADOSRESULT.STRING[4]);

            //Fecha Loading
            $ionicLoading.hide();

            //Chama funcao para carregar opções de tabela de preços
            cotacaoTabelaPrecos();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAODADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Dados


    /* Função que lista as opções de tabela de preço do cliente*/
    function cotacaoTabelaPrecos() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Tabelas de Preço....',
        duration: "60000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOTABELAPRECOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOTABELAPRECOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            for ( var i = 0; i < data.COTACAOTABELAPRECOSRESULT.STRING.length; i++ ) {

              var n = i - 1;

              $rootScope.condTabelaPrecosSeparado[ n ] = {

                id: data.COTACAOTABELAPRECOSRESULT.STRING[i].split(",")[0],
                nome: data.COTACAOTABELAPRECOSRESULT.STRING[i].split(",")[1]

              };

              //Preenche transportadora encontrada
              if (data.COTACAOTABELAPRECOSRESULT.STRING[i].split(",")[0] == $rootScope.cotacao.tabelaID){

                $rootScope.cotacao.tabelaDESC = data.COTACAOTABELAPRECOSRESULT.STRING[i].split(",")[1];

              }//End if transportadora

            }

            //Fecha Loading
            $ionicLoading.hide();

            //Chama função para carregar condições de pagamento
            cotacaoCondPagamento();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOTABELAPRECOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End cotacaoTabelaPrecos();


    /*Função que que lista as opções de condição de pagamento do cliente */
    function cotacaoCondPagamento() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Condições de Pagamento....',
        duration: "60000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOCONDPAGAMENTO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOCONDPAGAMENTORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            for ( var i = 0; i < data.COTACAOCONDPAGAMENTORESULT.STRING.length; i++ ) {

              var n = i - 1;

              $rootScope.condPgmtSeparado[ n ] = {

                id: data.COTACAOCONDPAGAMENTORESULT.STRING[i].split(",")[0],
                nome: data.COTACAOCONDPAGAMENTORESULT.STRING[i].split(",")[1]

              };

              //Preenche condição pagamento encontrada
              if (data.COTACAOCONDPAGAMENTORESULT.STRING[i].split(",")[0] == $rootScope.cotacao.condPAGTO){

                $rootScope.cotacao.condPAGTOdesc = data.COTACAOCONDPAGAMENTORESULT.STRING[i].split(",")[1];

              }//End if condição pagamento

              // alert($localstorage.get("CONDPAGTO"));

              // if (data.COTACAOCONDPAGAMENTORESULT.STRING[i].split(",")[0] == $localstorage.get("CONDPAGTO")){

              //   alert("OK");
              //   $rootScope.cotacao.condPAGTOdesc = data.COTACAOCONDPAGAMENTORESULT.STRING[i].split(",")[1];

              // }

            }

            //Fecha Loading
            $ionicLoading.hide();

            //Chama função para carregar transportadoras
            cotacaoTransportadora();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOCONDPAGAMENTORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End cotacaoCondPagamento();


    /* Função que lista as opções de transportadora do cliente*/
    function cotacaoTransportadora() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Transportadoras ...',
        duration: "60000"
      });

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDTIPO= CLIENTE" + 
                "&PESQPARAM=" + $localstorage.get('idCliente');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOTRANSPORTADORA.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOTRANSPORTADORARESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            for ( var i = 0; i < data.COTACAOTRANSPORTADORARESULT.STRING.length; i++ ) {

              var n = i - 1;

              $rootScope.transSeparado[ n ] = {

                id: data.COTACAOTRANSPORTADORARESULT.STRING[i].split(",")[0],
                nome: data.COTACAOTRANSPORTADORARESULT.STRING[i].split(",")[1]

              };

              //alert(data.COTACAOTRANSPORTADORARESULT.STRING[i].split(",")[0] + "-" + $rootScope.cotacao.transpID);

              //Preenche transportadora encontrada
              if (data.COTACAOTRANSPORTADORARESULT.STRING[i].split(",")[0] == $rootScope.cotacao.transpID){

                //alert("OK");

                $rootScope.cotacao.transpDESC = data.COTACAOTRANSPORTADORARESULT.STRING[i].split(",")[1];

              }//End if transportadora


            }

            //Fecha Loading
            $ionicLoading.hide();

            //Chama função para carregar parametros
            parametrosLista();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOTRANSPORTADORARESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
        .fail(function(data) {

          //Fecha o Loading
          $ionicLoading.hide();

          //Mensagem de Erro
          $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
      } );

    } //End cotacaoTransportadora();


    /*Função que lista os tipo de frete do cliente*/
    function parametrosLista() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Parâmetros ...',
        duration: "60000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&CPARAMETRO= 05" + 
                "&CFILTRO= ";

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/PARAMETROSDADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.PARAMETROSDADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            for ( var i = 0; i < data.PARAMETROSDADOSRESULT.STRING.length; i++ ) {

              var n = i - 1;

              $rootScope.paraListaSeparado[ n ] = {

                id: data.PARAMETROSDADOSRESULT.STRING[i].split(",")[0],
                nome: data.PARAMETROSDADOSRESULT.STRING[i].split(",")[1]

              };

            }

            //Fecha Loading
            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.PARAMETROSDADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End parametrosLista();


    /*Função que permite a alteração da cotação de um cliente */
    $scope.alterarCotacao = function() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      $localstorage.set('TABELAID', $rootScope.cotacao.tabelaID);


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get("IDCOTACAO") +
                "&IDCLIENTE=" + $scope.cotacaoCliente.id +
                "&IDCONDICAO=" + $rootScope.cotacao.condPAGTO +
                "&IDTABELAPRECO=" + $rootScope.cotacao.tabelaID +
                "&IDTRANSPORTADORA=" + $rootScope.cotacao.transpID +
                "&TIPOFRETE=" + $rootScope.cotacao.freteID +
                "&DESCONTO1=" + $localstorage.get("DESCONTO1") +
                "&DESCONTO2=" + $localstorage.get("DESCONTO2") +
                "&DESCONTO3=" + $localstorage.get("DESCONTO3") +
                "&DESCONTO4=" + $localstorage.get("DESCONTO4") +
                "&OBSERVACOES=" + $rootScope.cotacao.observacoes;

                //alert(vars);

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOATUALIZA.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOATUALIZARESULT.STRING[0];


          //Valida as condições
          if ($scope.status == "S"){

            //Fecha Loading
            $ionicLoading.hide();

            //Mensagem
            $cordovaToast.showShortBottom("Cotação atualizada com Sucesso!");

            //Direciona para revisar de itens cotacao
            $state.go("itensCotacao");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOATUALIZARESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    } //End alterarCotacao();

  } //End controller

} )();
