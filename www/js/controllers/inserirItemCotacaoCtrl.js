( function () {
  'use strict';

  angular.module( 'smartsales.controllers.inserirItemCotacaoCtrl', [] )

  /* @Controller*/
  .controller( "inserirItemCotacaoCtrl", inserirItemCotacaoCtrl );

  /* Injeção de dependência*/
  inserirItemCotacaoCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading","$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaGeolocation", "$cordovaDevice", "loginFactory", "$cordovaBarcodeScanner"
  ];

  /*Declara uma função para ser usado no controller 'inserirItemCotacaoCtrl' e com todas as funções do $scope Cliente*/
  function inserirItemCotacaoCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaGeolocation, $cordovaDevice, loginFactory, $cordovaBarcodeScanner) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Inserir Item Cotação');

      //Inicia o root produto 
      $rootScope.produto = {
        PRODUTOPESQ: ""
      };

      $scope.barcodeData = "";
      $scope.showAlert = showAlert;
      $scope.separadoItem = [];

    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert

    //Função que acessa a camera para escanear uma barra de códigos
    $scope.scan = function() {
      $cordovaBarcodeScanner
      .scan()
      .then( function ( barcodeData ) {
        $scope.produto.PRODUTOPESQ = barcodeData.text;
        $scope.listarProdutos();
      },
      function ( error ) {
        console.log( error );
      } );
    } //End scan();


    //Função listarProdutos
    $scope.listarProdutos = function() {
      
      //Valida se PRODUTOPESQ esta vazio
      if ( $scope.produto.PRODUTOPESQ == "" ) {

        //Mensagem de erro
        $scope.showAlert('Atenção', 'Por favor, insira dados no campo!' );

      } else {

        //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );


        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                  "&PRODUTOPESQ="+ $scope.produto.PRODUTOPESQ;

        
        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOBUSCAR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.PRODUTOBUSCARRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

              //Carrega informações de retorno
              for ( var i = 0; i < data.PRODUTOBUSCARRESULT.STRING.length; i++ ) {

                var n = i -1;

                $scope.separadoItem[n] = {
                  cod: data.PRODUTOBUSCARRESULT.STRING[i].split(",")[0],
                  nome: data.PRODUTOBUSCARRESULT.STRING[i].split(",")[1],
                  unid: data.PRODUTOBUSCARRESULT.STRING[i].split(",")[2],
                  outro: data.PRODUTOBUSCARRESULT.STRING[i].split(",")[3]
                };
              }

              $ionicLoading.hide();

              $scope.produto.PRODUTOPESQ = "";

            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro do WS
              var erro = data.PRODUTOBUSCARRESULT.STRING[1];
              $cordovaToast.showShortBottom(erro);

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );
          }

      $scope.produto.PRODUTOPESQ = "";
      //$scope.separado = [];

    }//End listarProdutos();


    /* Função que lista todos os produtos de acordo com o que o usário escreveu */
    $scope.listarProdutosDestaque = function() {

      //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );


        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO');

        
        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/PRODUTOSEMDESTAQUE.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.PRODUTOSEMDESTAQUERESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

              //Loop para ler as informações de retorno
              for ( var i = 0; i < data.PRODUTOSEMDESTAQUERESULT.STRING.length; i++ ) {

                var n = i - 1;

                $scope.separadoItem[ n ] = {
                  cod: data.PRODUTOSEMDESTAQUERESULT.STRING[i].split(",")[0],
                  nome: data.PRODUTOSEMDESTAQUERESULT.STRING[i].split(",")[1],
                  unid: data.PRODUTOSEMDESTAQUERESULT.STRING[i].split(",")[2],
                  outro: data.PRODUTOSEMDESTAQUERESULT.STRING[i].split(",")[3]
                };
              }

              //Fecha mensagem de loading
              $ionicLoading.hide();

            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro do WS
              var erro = data.PRODUTOSEMDESTAQUERESULT.STRING[1];
              $cordovaToast.showShortBottom(erro);

            }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //end listarProdutosDestaque();

    $scope.alteraProduto = function(id){


      $localstorage.set("idPROD", id);
      
      //Vai para tela de detalhe produto
      $state.go("alterarProdutoInterna");

    }//End salvaIDProduto


}} )();