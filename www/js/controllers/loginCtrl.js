( function () {
  'use strict';

  angular.module( 'smartsales.controllers.loginCtrl', [] )

  /* @Controller*/
  .controller( "loginCtrl", loginCtrl );

  /* Injeção de dependência*/
  loginCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading",
    "$state", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaGeolocation", "$cordovaDevice", "loginFactory"
  ];

  /*Declara uma função para ser usado no controller 'loginCtrl' e com todas as funções do $scope Cliente*/
  function loginCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaGeolocation, $cordovaDevice, loginFactory) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Login');

      //Define informações de configuração
      $localstorage.set("ws", "http://geracaomidia.com.br/app/smartsales");
      $localstorage.set("servidor", "focorio.ddns.net");
      $localstorage.set("porta", "10100");
      $localstorage.set("versao", "3.1.2");

      //Recebe informações do Aparelho e salva em localstorage
      $localstorage.set("SMARTMODELO", $cordovaDevice.getModel());
      $localstorage.set("SMARTFABRICANTE", $cordovaDevice.getPlatform());

      //Inicia informações de localização
      $localstorage.set('SMARTLATITUDE', "0");
      $localstorage.set('SMARTLONGITUDE', "0");


      //Valida se já existe algum vendedor salvo em localstorage
      if ($localstorage.get("VENDCOD") == null && $localstorage.get("VENDSENHA")== null){

        //Inicia o root login sem vendedor e senha
        $rootScope.login = {
          VENDCOD: "",
          VENDSENHA: ""
        };

      } else {

        //Inicia o root login com vendedor e senha
        $rootScope.login = {
          VENDCOD: $localstorage.get("VENDCOD"),
          VENDSENHA: ""
        };

        //Chama função para validar vendedor
        $scope.validaVendedor();

      }

    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Recebe localização atual do vendedor e salva em localstorage
    function getLocation() {
      var posOptions = {
        timeout: 10000,
        enableHighAccuracy: false
      };
      $cordovaGeolocation
        .getCurrentPosition( posOptions )
        .then( function ( position ) {

          //Recebe latitude e longitude
          var lat = position.coords.latitude;
          var long = position.coords.longitude;

          //Salva a localização em localstorage
          $localstorage.set('SMARTLATITUDE', lat);
          $localstorage.set('SMARTLONGITUDE', long);

        }, function ( err ) {
          //showAlert("Erro GPS","Não foi possível obter localização. Verifique se o GPS esta ativado e tente novamente!");
        } );
    }//Fim geoLocation;


    //Valida Vendedor e faz login
    $scope.validaVendedor = function(){

      //Chama função de localização
      getLocation();


      //Mensagem após clicar em Entrar
      $ionicLoading.show( {
        template: 'Validando....',
        duration: "6000"
      } );

      //Configura variaveis do POST
      var vars = "VENDCOD="+ $scope.login.VENDCOD + 
                "&VENDSENHA="+ $scope.login.VENDSENHA + 
                "&VENDLIC="+ $localstorage.get("licenca") + 
                "&SMARTMODELO="+ $localstorage.get("SMARTMODELO") + 
                "&SMARTFABRICANTE="+ $localstorage.get("SMARTFABRICANTE") + 
                "&SMARTUSUARIO="+ "" + 
                "&SMARTSALESVERSAO="+ $localstorage.get("versao") + 
                "&SMARTLATITUDE="+ $localstorage.get("SMARTLATITUDE") + 
                "&SMARTLONGITUDE="+ $localstorage.get("SMARTLONGITUDE");

      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/VENDEDORVALIDAR.php", vars)
        .done(function(data){

          //Salva informação de login
          $localstorage.set('VENDCOD', $scope.login.VENDCOD);
          $localstorage.set('VENDSENHA', $scope.login.VENDSENHA);

          //Recebe o status de retorno
          var status = data.VENDEDORVALIDARRESULT.STRING[0];

          //Valida as condições
          if (status == "S"){

            //Salva informações em localstorage
            $localstorage.set('loginEMPRESA', data.VENDEDORVALIDARRESULT.STRING[1]);
            $localstorage.set('loginMSG1', data.VENDEDORVALIDARRESULT.STRING[2]);
            $localstorage.set('loginMSG2', data.VENDEDORVALIDARRESULT.STRING[3]);
            $localstorage.set('loginCONEXAO', data.VENDEDORVALIDARRESULT.STRING[4]);


            //Salva lista de empresas em localstorage
            //Criar Array
            var empresaLista = [];

            for ( var i = 5; i < data.VENDEDORVALIDARRESULT.STRING.length; i++ ) {

              //Define variavel para contar empresas
              var n = i - 5;

              //Armazena em localstorage
              loginFactory.vendedorInformacao[n] = {
                idEmpresa: data.VENDEDORVALIDARRESULT.STRING[i].split(",")[0],
                licenca: data.VENDEDORVALIDARRESULT.STRING[i].split(",")[1],
                outro: data.VENDEDORVALIDARRESULT.STRING[i].split(",")[2]
              };

              $localstorage.set("vendedorInformacao", loginFactory.vendedorInformacao);

            } //End for

            //Fecha o Loading
            $ionicLoading.hide();

            //Vai para tela de menu
            $state.go( "inicio" );

          } else if (status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.VENDEDORVALIDARRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

        } )
        .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//Fim validaVendedor

}} )();