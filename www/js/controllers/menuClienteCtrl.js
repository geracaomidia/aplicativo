( function () {
  'use strict';

  angular.module( 'smartsales.controllers.menuClienteCtrl', [] )

  /* @Controller*/
  .controller( "menuClienteCtrl", menuClienteCtrl );

  /* Injeção de dependência*/
  menuClienteCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading", "$state", "$ionicActionSheet", "$q", "$timeout",
    "$routeParams", "$stateParams", "$cordovaToast", "$localstorage"
  ];

  /*Declara uma função para ser usado no controller 'menuClienteCtrl' e com todas as funções do $scope Cliente*/
  function menuClienteCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, $ionicActionSheet, $q, $timeout, $routeParams, $stateParams, $cordovaToast, $localstorage) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Menu Cliente');

      $scope.showAlert = showAlert;
      $scope.cotacaoEmAbertoEspecifico = [];
      var pageLoad = true;

      //Chama função para consultar cotações em aberto
      cotacaoEmAberto();


    }//Fim onDeviceReady


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert

    //Push de cotações em aberto
    $scope.doRefreshMenu = function () {
      $scope.cotacaoEmAbertoParam();
      $scope.$broadcast('scroll.refreshComplete');
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "3000"
      } );
    };


    //Busca cotações cliente no PUSH
    $scope.cotacaoEmAbertoParam = function() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCLIENTE=" + $localstorage.get('idCliente');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOEMABERTO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOEMABERTORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.COTACAOEMABERTORESULT.STRING.length; i++ ) {

              var n = i -1;

              $scope.cotacaoEmAbertoEspecifico[n] = {
                idCotacao: data.COTACAOEMABERTORESULT.STRING[i].split(",")[0],
                emissao: data.COTACAOEMABERTORESULT.STRING[i].split(",")[1],
                idCliente: data.COTACAOEMABERTORESULT.STRING[i].split(",")[2],
                fantasia: data.COTACAOEMABERTORESULT.STRING[i].split(",")[3],
                totalGeral: data.COTACAOEMABERTORESULT.STRING[i].split(",")[4],
                qtdItens: data.COTACAOEMABERTORESULT.STRING[i].split(",")[5]
              };
            }

            //Fecha o loading
            $ionicLoading.hide();


          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOEMABERTORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } //End cotacaoEmAbertoParam();


    //Consulta cotações em Aberto ao abrir a view
    function cotacaoEmAberto(){
      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCLIENTE=" + $localstorage.get('idCliente');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOEMABERTO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOEMABERTORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.COTACAOEMABERTORESULT.STRING.length; i++ ) {

              var n = i -1;

              $scope.cotacaoEmAbertoEspecifico[n] = {
                idCotacao: data.COTACAOEMABERTORESULT.STRING[i].split(",")[0],
                emissao: data.COTACAOEMABERTORESULT.STRING[i].split(",")[1],
                idCliente: data.COTACAOEMABERTORESULT.STRING[i].split(",")[2],
                fantasia: data.COTACAOEMABERTORESULT.STRING[i].split(",")[3],
                totalGeral: data.COTACAOEMABERTORESULT.STRING[i].split(",")[4],
                qtdItens: data.COTACAOEMABERTORESULT.STRING[i].split(",")[5]
              };
            }

            $ionicLoading.hide();


          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAOEMABERTORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End cotacaoEmAberto


    /*Abre opções para cotação*/
    $scope.opcoesCotacao = function(idCotacao) {

      //Salva i ID da Cotação
      $localstorage.set("IDCOTACAO", idCotacao);

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
            text: '<i class="fa  fa-edit"></i> Alterar Cotação'
          },
          {
            text: '<i class="fa  fa-clock-o"></i> Resumo Cotação'
          },{
            text: '<i class="fa  fa-chevron-right"></i> Encerrar / Transmitir'
          },{
            text: '<i class="fa  fa-trash"></i> Excluir Cotação'
          }
        ],
        cancel: function () {
          
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {

            //Chama funcao para gravar cotação
            //cotacaoPedidoGravar();
            cotacaoDadosListar();

          } else if ( index == '1' ) {

            //Chama funcao para listar cotacao
            //cotacaoDadosListar();
            $state.go("resumoCotacao");

          } else if ( index == '2' ) {

            //$state.go("descontos");
            cotacaoPedidoGravar();

          } else {

            //Chama funcao para excluir cotacao
            excluirCotacao();

          }
          return true;
        }
      } );
    } //End opcoesCotacao;


    /*Função que transforma uma cotação em pedido de venda*/
    function cotacaoPedidoGravar() {

      //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );


        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

        
        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/COTACAOPEDIDOGRAVAR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.COTACAOPEDIDOGRAVARRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

              //Fecha o Loading
              $ionicLoading.hide();

              $cordovaToast.showShortBottom("Cotação salva com Sucesso!");

              //Chama função para consultar cotações em aberto
              cotacaoEmAberto();

            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              $cordovaToast.showShortBottom("Erro ao salvar a cotação!");

              //Chama função para consultar cotações em aberto
              cotacaoEmAberto();

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );

    } // End cotacaoPedidoGravar;


    /*Função que permite a exclusão de uma cotação de um cliente*/
    function excluirCotacao() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO="+ $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOEXCLUIR.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOEXCLUIRRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Cotação excluída com Sucesso!");

            //Chama função para consultar cotações em aberto
          cotacaoEmAberto();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Erro ao excluir a cotação!");

            //Chama função para consultar cotações em aberto
            cotacaoEmAberto();

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    } // End excluirCotacao();

    //Função cotacaoDados
    function cotacaoDadosListar(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: 'Carregando Cotação....',
        duration: "6000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCOTACAO=" + $localstorage.get('IDCOTACAO');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAODADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAODADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Salva em localstorage informações da cotacao
            $localstorage.set("DESCONTO1", data.COTACAODADOSRESULT.STRING[7]);
            $localstorage.set("DESCONTO2", data.COTACAODADOSRESULT.STRING[8]);
            $localstorage.set("DESCONTO3", data.COTACAODADOSRESULT.STRING[9]);
            $localstorage.set("DESCONTO4", data.COTACAODADOSRESULT.STRING[10]);

            $localstorage.set("acaoCOTACAO", "Editar");

            //Fecha o Loading
            $ionicLoading.hide();

            //Direciona para revisar de cotacao
            $state.go("addCotacao");

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.COTACAODADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    }//End Cotacao Dados


    $scope.novaCotacao = function(){

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCLIENTE="+ $localstorage.get('idCliente');

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/COTACAOINCLUIR.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.COTACAOINCLUIRRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Salva informações em localstorage
            $localstorage.set("IDCOTACAO", data.COTACAOINCLUIRRESULT.STRING[1]);
            $localstorage.set("DESCONTO1", data.COTACAOINCLUIRRESULT.STRING[2]);
            $localstorage.set("DESCONTO2", data.COTACAOINCLUIRRESULT.STRING[3]);
            $localstorage.set("DESCONTO3", data.COTACAOINCLUIRRESULT.STRING[4]);
            $localstorage.set("DESCONTO4", data.COTACAOINCLUIRRESULT.STRING[5]);
            $localstorage.set("TRANSPID", data.COTACAOINCLUIRRESULT.STRING[6]);
            $localstorage.set("TRANSPDESC", data.COTACAOINCLUIRRESULT.STRING[7]);
            $localstorage.set("TABELAID", data.COTACAOINCLUIRRESULT.STRING[8]);
            $localstorage.set("TABELADESC", data.COTACAOINCLUIRRESULT.STRING[9]);
            $localstorage.set("FRETEID", data.COTACAOINCLUIRRESULT.STRING[10]);
            $localstorage.set("FRETEDESC", data.COTACAOINCLUIRRESULT.STRING[11]);
            $localstorage.set("CONDPAGTO", data.COTACAOINCLUIRRESULT.STRING[12]);

            //Fecha o Loading
            $ionicLoading.hide();

            //Direciona para revisar de cotacao
            $state.go( "addCotacao" );

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom("Não foi possível adicionar uma Nova Cotação. Tente Novamente!");

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    }//End novaCotacao

}} )();