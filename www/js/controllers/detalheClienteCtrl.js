( function () {
  'use strict';

  angular.module( 'smartsales.controllers.detalheClienteCtrl', [ 'ngCordova', "ui.router" ] )

  /* @Controller*/
  .controller( "detalheClienteCtrl", detalheClienteCtrl );

  /* Injeção de dependência*/
  detalheClienteCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$cordovaBarcodeScanner", "$ionicPlatform", "$stateParams", "$ionicActionSheet", "$localstorage", "$cordovaToast"
  ];

  /*  Declara uma função para ser usado no controller 'produtoCtrl' e com todas as funções do $scope Cliente*/
  function detalheClienteCtrl($ionicPopup, $rootScope, $scope, $state, $http, $ionicLoading, $cordovaBarcodeScanner, $ionicPlatform, $stateParams, $ionicActionSheet, $localstorage, $cordovaToast ) {

    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Detalhes Cliente');

      $rootScope.clienteDados = [];
      $scope.IDCLIENTE = $localstorage.get("idCliente");
      $rootScope.cliente = {
        RAZAOSOCIAL: "",
        TELEFONE: "",
        EMAIL: ""
      };

      //Chama funcao para buscar informações do cliente
      clienteEspecifico();

    }//End onDeviceReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    //Consulta Detalhes Cliente
    function clienteEspecifico() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCLIENTE=" + $localstorage.get('idCliente'); 

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/CLIENTEDADOS.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.CLIENTEDADOSRESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Carrega informações de retorno
            for ( var i = 0; i < data.CLIENTEDADOSRESULT.STRING.length; i++ ) {
             
              $rootScope.clienteDados[ i ] = data.CLIENTEDADOSRESULT.STRING[ i ].split( "," );
              
              $rootScope.clienteDados[ i ] = $rootScope.clienteDados[ i ].toString();

              //Valida Razao Social
              if (i == 2){
                $rootScope.cliente.RAZAOSOCIAL = $rootScope.clienteDados[ i ] = $rootScope.clienteDados[ i ].toString();
              }

              //Valida Telefone
              if (i == 10){
                $rootScope.cliente.TELEFONE = $rootScope.clienteDados[ i ] = $rootScope.clienteDados[ i ].toString();
              }

              //Valida Email
              if (i == 11){
                $rootScope.cliente.EMAIL = $rootScope.clienteDados[ i ] = $rootScope.clienteDados[ i ].toString();
              }
            }


            //Fecha o Loading
            $ionicLoading.hide();

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.CLIENTEDADOSRESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    } //End clienteEspecifico();


    /* Função que atualiza as informações do cliente escolhido */
    $scope.clienteAtualizaCadastro = function() {

      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                "&IDCLIENTE=" + $localstorage.get('idCliente') +
                "&NEWRAZAOSOCIAL=" + $rootScope.cliente.RAZAOSOCIAL +
                "&NEWTELEFONE=" + $rootScope.cliente.TELEFONE +
                "&NEWEMAIL=" + $rootScope.cliente.EMAIL;

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/CLIENTEATUALIZACADASTRO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data.CLIENTEATUALIZACADASTRORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom( 'Dados salvos com Sucesso!' );

            //Atualiza a pagina
            $window.location.reload(true);

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            $cordovaToast.showShortBottom( 'Erro ao salvar os dados. Tente novamente!' );

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );
    } //End clienteAtualizaCadastro();

  } //End controller

} )();
