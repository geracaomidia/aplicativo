( function () {
  'use strict';

  angular.module( 'smartsales.controllers.actionMenuAlterarCotacao', [ "ui.router" ] )

  // Show the action sheet Alterar cotação
  .controller( "actionMenuAlterarCotacao", actionMenuAlterarCotacao );

  /*
   * Injeção de dependência
   */
  actionMenuAlterarCotacao.$inject = [ "$scope", "$ionicActionSheet", "$timeout", "$state" ];

  function actionMenuAlterarCotacao( $scope, $ionicActionSheet, $timeout, $state ) {

    /*
     * Deprecated
     */
    $scope.show = function () {

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
          text: 'Itens da Cotação'
        }, {
          text: 'Descontos'
        }, {
          text: 'Resumo da Cotação'
        }, {
          text: 'Ajuda'
        } ],
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {
            $state.go( "itensCotacao" );

          } else if ( index == '1' ) {
            $state.go( "descontos" );

          } else if ( index == '2' ) {
            $state.go( "resumoCotacao" );

          } else {
            // $state.go("");
            alert( "Ajuda" );
          }
          return true;
        }
      } );
    };

  }

} )();
