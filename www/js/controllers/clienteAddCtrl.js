( function () {
  'use strict';

  angular.module( 'smartsales.controllers.clienteAddCtrl', [ 'ngCordova', "angularSoap", "ui.router" ] )

  .controller( "clienteAddCtrl", clienteAddCtrl );

  /* Injeção de dependência*/
  clienteAddCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading", "loginFactory", "soapFactory" ];

  function clienteAddCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, loginFactory, soapFactory ) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Adicionar Cliente');

      $scope.cliente = {
        NEWTIPOCLIENTE: "",
        NEWTIPOPESSOA: "",
        NEWCNPJ: "",
        NEWRAZAOSOCIAL: "",
        NEWINSCRICAOESTADUAL: "",
        NEWSIMPLESNACIONAL: "",
        NEWTELEFONE: "",
        NEWTELEFONE2: "",
        NEWEMAIL: "",
        NEWCONTATONOME: ""
      };

      $scope.separado = [];

    }//END onDeviceReady


    //Define show Alert
    $scope.showAlert = function ( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    };

    
    /* Função que adiciona um cliente*/
    $scope.adicionarCliente = function () {


      //Mensagem após clicar em Buscar
      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );


      //Configura variaveis do POST
      var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') + 
                "&NEWTIPOCLIENTE=" + $scope.cliente.NEWTIPOCLIENTE +
                "&NEWTIPOPESSOA=" + $scope.cliente.NEWTIPOPESSOA +
                "&NEWCNPJ=" + $scope.cliente.NEWCNPJ +
                "&NEWRAZAOSOCIAL=" + $scope.cliente.NEWRAZAOSOCIAL +
                "&NEWINSCRICAOESTADUAL=" + $scope.cliente.NEWINSCRICAOESTADUAL +
                "&NEWSIMPLESNACIONAL=" + $scope.cliente.NEWSIMPLESNACIONAL +
                "&NEWTELEFONE=" + $scope.cliente.NEWTELEFONE +
                "&NEWTELEFONE2=" + $scope.cliente.NEWTELEFONE2 +
                "&NEWEMAIL=" + $scope.cliente.NEWEMAIL +
                "&NEWCONTATONOME=" + $scope.cliente.NEWCONTATONOME;

      
      //Faz POST no WS
      $.post("http://geracaomidia.com.br/app/smartsales/CLIENTENOVOCADASTRO.php", vars)
        .done(function(data){

          //Recebe o status de retorno
          $scope.status = data. data.CLIENTENOVOCADASTRORESULT.STRING[0];

          //Valida as condições
          if ($scope.status == "S"){

            for ( var i = 0; i < data.CLIENTENOVOCADASTRORESULT.STRING.length; i++ ) {

              $scope.separado[ i ] = data.CLIENTENOVOCADASTRORESULT.STRING[ i ].split( "," );

            }

            $ionicLoading.hide();

            if ( $scope.separado[ 0 ] == "S" ) {

              $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );

            } else {

              $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] + " - \n" + $scope.separado[ 3 ] );

            }

          } else if ($scope.status == "N"){

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro do WS
            var erro = data.CLIENTENOVOCADASTRORESULT.STRING[1];
            $cordovaToast.showShortBottom(erro);

          }//End IF Valida condição

          } )
          .fail(function(data) {

            //Fecha o Loading
            $ionicLoading.hide();

            //Mensagem de Erro
            $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
        } );

    }; //end adicionarCliente();

  }

} )();
