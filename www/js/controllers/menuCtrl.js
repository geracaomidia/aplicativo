( function () {
  'use strict';

  angular.module( 'smartsales.controllers.menuCtrl', [] )

  .controller( 'menuCtrl', menuCtrl );

  /* Injeção de dependência */
  menuCtrl.$inject = ["$ionicPopup", "$rootScope", "$scope", "$http", "$ionicLoading",
    "$state", "loginFactory", "$ionicModal", "$cordovaToast", "$localstorage", "$window", "$cordovaGeolocation"
  ];

  /* Deprecated */
  function menuCtrl( $ionicPopup, $rootScope, $scope, $http, $ionicLoading, $state, loginFactory, $ionicModal, $cordovaToast, $localstorage, $window, $cordovaGeolocation ) {
    
    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //alert($localstorage.get('loginCONEXAO'));

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Menu');

      //Define root do Menu
      $rootScope.menu = {
        empresa: $localstorage.get("loginEMPRESA"),
        msg1: $localstorage.get("loginMSG1"),
        msg2: $localstorage.get("loginMSG2"),

        empresaSelect: loginFactory.vendedorInformacao[0].idEmpresa,

        //Informacoes das empresas
        idEmpresa1: loginFactory.vendedorInformacao[0].idEmpresa,
        idEmpresa2: loginFactory.vendedorInformacao[1].idEmpresa,
        licenca1: loginFactory.vendedorInformacao[0].licenca,
        licenca2: loginFactory.vendedorInformacao[1].licenca,
        outro1: loginFactory.vendedorInformacao[0].outro,
        outro2: loginFactory.vendedorInformacao[1].outro
      };

    }//Fim onDeviceReady

      // $scope.playlists = [
      //   { title: 'Reggae', id: 1 },
      //   { title: 'Chill', id: 2 },
      //   { title: 'Dubstep', id: 3 },
      //   { title: 'Indie', id: 4 },
      //   { title: 'Rap', id: 5 },
      //   { title: 'Cowbell', id: 6 }
      // ];
      //console.log($scope.playlists);
    //Salva empresa selecionada em localstorage
   // $localstorage.set("loginEMPRESASELECT", $scope.menu.empresaSelect);


    //Função para sair da aplicação
    $scope.sair = function(){
      ionic.Platform.exitApp();
    }//End Sair


    $scope.showAlert = showAlert;

    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert

  }//Fim menuCtrl

} )();
