( function () {
  'use strict';

  angular.module( 'smartsales.controllers.produtoCtrl', [ 'ngCordova', "ui.router" ] )

  /*
   * @Controller
   */
  .controller( "produtoCtrl", produtoCtrl );

  /*
   * Injeção de dependência
   */
  produtoCtrl.$inject = [ "cotacaoFactory", "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$cordovaBarcodeScanner", "$ionicPlatform", "$stateParams", "loginFactory",
    "soapFactory", "$ionicActionSheet"
  ];

  /*
   *  Declara uma função para ser usado no controller 'produtoCtrl' e com todas as funções do $scope Cliente
   */
  function produtoCtrl( cotacaoFactory, $ionicPopup, $rootScope, $scope, $state, $http,
    $ionicLoading, $cordovaBarcodeScanner, $ionicPlatform, $stateParams, loginFactory,
    soapFactory, $ionicActionSheet ) {

    // var url = 'http://smartsalesserver.herokuapp.com/produtobuscar';

    //Variables
    $scope.data = {
      IDEMPRESA: loginFactory.idEmpresa,
      IDCONEXAO: loginFactory.idconexao,
      produtoId: $stateParams.itemId,
      clienteId: $stateParams.clienteId,
      cotacaoId: $stateParams.cotacaoId
    };

    $rootScope.cotacaoDados = {
      produtoId: cotacaoFactory.produtoId,
      idCotacao: cotacaoFactory.idCotacao,
      idCliente: cotacaoFactory.idCliente,
      nomeCliente: cotacaoFactory.nomeCliente,
      condPgmt: cotacaoFactory.condPgmt,
      tabelaPreco: cotacaoFactory.tabelaPreco,
      transportadora: cotacaoFactory.transportadora,
      tipoFrete: cotacaoFactory.tipoFrete,
      desconto1: cotacaoFactory.desconto1,
      desconto2: cotacaoFactory.desconto2,
      desconto3: cotacaoFactory.desconto3,
      desconto4: cotacaoFactory.desconto4,
      idconexao: loginFactory.idconexao,
      idEmpresa: loginFactory.idEmpresa,
      produtoNome: cotacaoFactory.produtoNome,
      produtoEstoque: cotacaoFactory.produtoEstoque,
      produtoTransito: cotacaoFactory.produtoTransito,
      produtoQuantDisp: cotacaoFactory.produtoQuantDisp,
      produtoQuantCot: 1 || cotacaoFactory.produtoQuantCot,
      produtoPrecoUnit: cotacaoFactory.produtoPrecoUnit,
      produtoDescontoPerc: cotacaoFactory.produtoDescontoPerc,
      produtoValorTotal: cotacaoFactory.produtoValorTotal
    };

    $scope.barcodeData = "";

    $scope.separado = [];
    $scope.separadoItem = [];
    $rootScope.produtoData = [];
    $rootScope.produtoEstoque = [];
    $rootScope.precoUnitSeparado = [ {} ];

    //inserir produto
    $scope.cotTabelaPrecos = [];
    $scope.cotTabelPrecosSeparado = [];
    $scope.cotacaoTabelaPrecos = cotacaoTabelaPrecos;
    $scope.precoUnit = [];

    //Function calls
    $scope.scan = scan;
    $scope.showAlert = showAlert;
    $scope.listarProdutos = listarProdutos;
    $scope.listarProdutosDestaque = listarProdutosDestaque;
    $scope.produtoEspecifico = produtoEspecifico;
    $scope.produtoEstoqueDet = produtoEstoqueDet;
    $scope.precoUnitario = precoUnitario;
    $scope.calculaTotalProduto = calculaTotalProduto;
    $scope.showAlert = showAlert;
    $scope.actionMenuItensCotacao = actionMenuItensCotacao;
    $scope.cotacaoItemExcluir = cotacaoItemExcluir;
    $scope.cotacaoListaTodosItens = cotacaoListaTodosItens;
    $scope.produtoEspecificoSemRe = produtoEspecificoSemRe;
    $scope.produtoEstoqueDetSemRE = produtoEstoqueDetSemRE;

    /*
     * Abre um menu ao clicar em itens da cotação com opções de excluir & alterar
     */
    function actionMenuItensCotacao( produtoId, itemId ) {
      console.log( "ProdutoId: " + produtoId );
      console.log( "cotacaoId" + $stateParams.cotacaoId );
      cotacaoFactory.produtoId = produtoId;
      cotacaoFactory.itemId = itemId;

      var hideSheet = $ionicActionSheet.show( {
        buttons: [ {
          text: '<i class="fa fa-edit"></i>  Alterar item'
        }, {
          text: '<i class="fa fa-times"></i> Excluir item'
        } ],
        cancel: function () {
          //   hideSheet();
        },
        buttonClicked: function ( index ) {
          if ( index == '0' ) {
            $state.go( "alterarProdutoInterna", {
              clienteId: $stateParams.clienteId,
              cotacaoId: $stateParams.cotacaoId,
              produtoId: produtoId
            } );
            produtoEspecificoSemRe( produtoId );
          }
          if ( index == '1' ) {
            cotacaoItemExcluir( itemId );
            //    cotacaoListaTodosItens();
          }
          return true;
        }
      } );
    } //End actionMenuItensCotacao

    /*
     * Função que faz a exclusão de um item de uma cotação
     */
    function cotacaoItemExcluir( produtoId ) {

      var url = soapFactory.url + "/COTACAOITEMEXCLUIR.php";
      var soapAction = soapFactory.soapAction + "/COTACAOITEMEXCLUIR";

      $http.post( url, {
          "IDEMPRESA": loginFactory.idEmpresa,
          "IDCONEXAO": loginFactory.idconexao,
          "IDCOTACAO": $stateParams.cotacaoId,
          "IDITEM": produtoId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data, status, headers, config ) {
          for ( var i = 0; i < data.COTACAOITEMEXCLUIRRESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.COTACAOITEMEXCLUIRRESULT.STRING[ i ].split( "," );
            console.log( "cotacaoItemExcluir " + $scope.separado[ i ] );
          }
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            $cordovaToast.showLongBottom( "Item excluida com sucesso!" );
          } else {
            $scope.showAlert( $scope.separado[ 1 ], $scope.separado[ 2 ] );
          }
        } )
        .error( function ( data, status, headers, config ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        } );
    } //End cotacaoItemExcluir();

    /*
     * Função que lista todos os itens default da cotação
     */
    function cotacaoListaTodosItens() {

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
      } );

      var url = soapFactory.url + "/COTACAOLISTAITENS.php";
      var soapAction = soapFactory.soapAction + "/COTACAOLISTAITENS";

      $http.post( url, {
          "IDEMPRESA": loginFactory.idEmpresa,
          "IDCONEXAO": loginFactory.idconexao,
          "IDCOTACAO": $stateParams.cotacaoId
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.COTACAOLISTAITENSRESULT.STRING.length; i++ ) {
            $scope.cotacaoListaItens[ i ] = data.COTACAOLISTAITENSRESULT.STRING[ i ].split( "," );
            console.log( "listaItens" + [ i ] + " = " + $scope.cotacaoListaItens[ i ] );
            // $scope.cotacaoListaItens[i] = $scope.cotacaoListaItens[i].toString();
            var n = i - 1;
            cotacaoFactory.listaItensSeparado[ n ] = {
              item: $scope.cotacaoListaItens[ i ][ 0 ],
              idProduto: $scope.cotacaoListaItens[ i ][ 1 ],
              quantidade: $scope.cotacaoListaItens[ i ][ 2 ],
              // naoSei : $scope.cotacaoListaItens[i][3],
              valorTotal: $scope.cotacaoListaItens[ i ][ 4 ],
              descricaoProduto: $scope.cotacaoListaItens[ i ][ 5 ]
            };
            $rootScope.cotacaoListaItensSeparado[ n ] = {
              item: $scope.cotacaoListaItens[ i ][ 0 ],
              idProduto: $scope.cotacaoListaItens[ i ][ 1 ],
              quantidade: $scope.cotacaoListaItens[ i ][ 2 ],
              valorTotal: $scope.cotacaoListaItens[ i ][ 4 ],
              descricaoProduto: $scope.cotacaoListaItens[ i ][ 5 ]
            };
          }
          $ionicLoading.hide();
          if ( $scope.cotacaoListaItens[ 0 ] == "S" ) {
            //Go
          } else {
            $cordovaToast.showShortBottom( $scope.cotacaoListaItens[ 1 ].toString() )
              .then( function ( success ) {
                console.log( "Toast success" );
              }, function ( error ) {
                console.log( "Toast failed" );
              } );
            // $scope.showAlert($scope.cotacaoListaItens);
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );
    } //End cotacaoListaTodosItens();

    /*
     * Função que lista as informações detalhadas de um produto
     */
    function produtoEstoqueDet( idProduto ) {
      alert(idProduto);
      //Url to to make the soap call
      var url = soapFactory.url + "/PRODUTOESTOQUE.php";
      var soapAction = soapFactory.soapAction + "/PRODUTOESTOQUE";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "PRODUTOCOD": idProduto
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.PRODUTOESTOQUERESULT.STRING.length; i++ ) {
            $rootScope.produtoEstoque[ i ] = data.PRODUTOESTOQUERESULT.STRING[ i ].split( "," );
            //console.log("produtoEstoque"+ [i] + " = " + $rootScope.produtoEstoque[i]);
            $rootScope.produtoEstoque[ i ] = $rootScope.produtoEstoque[ i ].toString();
          }
          cotacaoFactory.produtoNome = $rootScope.produtoEstoque[ 8 ];
          cotacaoFactory.produtoEstoque = $rootScope.produtoEstoque[ 2 ];
          cotacaoFactory.produtoTransito = $rootScope.produtoEstoque[ 5 ];
          cotacaoFactory.produtoQuantDisp = $scope.produtoEstoque[ 6 ];
          // Função para definir o preço uitário do produto
          precoUnitario( idProduto );
          if ( !$rootScope.precoUnitSeparado[ 0 ].valor ) {
            $rootScope.precoUnitSeparado[ 0 ].valor = "Sem valor!";
          }
          // CHAMA A FUNCAO PRODUTO ESPECIFICO E REDIRECIONA DENTRO DA FUNCAO PARA A PAGINA inserir-produto-interna.html
          // Primeiro ele define o estoque e se está disponível
          // Grava as variáveis do produto e chama a funcao para o produto específico
          produtoEspecifico( idProduto );
          $ionicLoading.hide();
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );
    } //End produtoEstoqueDet

    /*
     * Função que lista as informações detalhadas de um produto
     */
    function produtoEstoqueDetSemRE( idProduto ) {
      //Url to to make the soap call
      var url = soapFactory.url + "/PRODUTOESTOQUE.php";
      var soapAction = soapFactory.soapAction + "/PRODUTOESTOQUE";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "PRODUTOCOD": idProduto
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.PRODUTOESTOQUERESULT.STRING.length; i++ ) {
            $rootScope.produtoEstoque[ i ] = data.PRODUTOESTOQUERESULT.STRING[ i ].split( "," );
            //console.log("produtoEstoque"+ [i] + " = " + $rootScope.produtoEstoque[i]);
            $rootScope.produtoEstoque[ i ] = $rootScope.produtoEstoque[ i ].toString();
          }
          cotacaoFactory.produtoNome = $rootScope.produtoEstoque[ 8 ];
          cotacaoFactory.produtoEstoque = $rootScope.produtoEstoque[ 2 ];
          cotacaoFactory.produtoTransito = $rootScope.produtoEstoque[ 5 ];
          cotacaoFactory.produtoQuantDisp = $scope.produtoEstoque[ 6 ];
          // Função para definir o preço uitário do produto
          precoUnitario( idProduto );
          if ( !$rootScope.precoUnitSeparado[ 0 ].valor ) {
            $rootScope.precoUnitSeparado[ 0 ].valor = "Sem valor!";
          }
          // CHAMA A FUNCAO PRODUTO ESPECIFICO E REDIRECIONA DENTRO DA FUNCAO PARA A PAGINA inserir-produto-interna.html
          // Primeiro ele define o estoque e se está disponível
          // Grava as variáveis do produto e chama a funcao para o produto específico
          //produtoEspecificoSemRe( idProduto );
          $ionicLoading.hide();
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );
    } //End produtoEstoqueDet

    /* Função que acessa a camera para escanear uma barra de códigos*/
    function scan() {
      $cordovaBarcodeScanner
        .scan()
        .then( function ( barcodeData ) {
            $scope.data.PRODUTOPESQ = barcodeData.text;
            listarProdutos();
            // alert("Escaneado!\n" +
            //     "Resultado: " + barcodeData.text + "\n" +
            //     "Formato: " + barcodeData.format + "\n" +
            //     "Cancelado: " + barcodeData.cancelled);
          },
          function ( error ) {
            console.log( error );
          } );

      //NOTE: encoding not functioning yet
      // $cordovaBarcodeScanner
      //     .encode(BarcodeScanner.Encode.TEXT_TYPE, "http://www.nytimes.com")
      //     .then(function(success) {
      //         // Success!
      //   },
      //   function(error) {
      //       // An error occurred
      //   });

    } //End scan();

    /*
     * Função que acessa a camera para escanear uma barra de códigos
     */
    function scanInserirProduto() {
      $cordovaBarcodeScanner
        .scan()
        .then( function ( barcodeData ) {
            $scope.data.PRODUTOPESQ = barcodeData.text;
            listarProdutos();
          },
          function ( error ) {
            console.log( error );
          } );

    } //End scan();

    /*
     * Função que permite a criação de um alerta user-friendly
     */
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        //console.log('Pós log');
      } );
    } //End showAlert();

    /*
     * Função que lista todos os produtos de acordo com o que o usário escreveu
     */
    function listarProdutosDestaque() {

      //Url to to make the soap call
      var url = soapFactory.url + "/PRODUTOSEMDESTAQUE.php";
      var soapAction = soapFactory.soapAction + "/PRODUTOSEMDESTAQUE";


      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          //console.log(data);
          for ( var i = 0; i < data.PRODUTOSEMDESTAQUERESULT.STRING.length; i++ ) {
            $scope.separado[ i ] = data.PRODUTOSEMDESTAQUERESULT.STRING[ i ].split( "," );
            console.log( $scope.separado[ i ] );
            var n = i - 1;
            $scope.separadoItem[ n ] = {
              cod: $scope.separado[ i ][ 0 ],
              nome: $scope.separado[ i ][ 1 ],
              unid: $scope.separado[ i ][ 2 ],
              outro: $scope.separado[ i ][ 3 ]
            };
          }
          $ionicLoading.hide();

          if ( $scope.separado[ 0 ] == "S" ) {
            //Go
          } else {
            $scope.showAlert( "Smartsales", $scope.separado[ 1 ] );
          }
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );


      $scope.data.PRODUTOPESQ = "";
      $scope.separado = [];

    } //end listarProdutosDestaque();

    // function listarProdutos() {
    //   //Url to to make the soap call
    //   var url = soapFactory.url + "/PRODUTOBUSCAR.php";
    //   var soapAction = soapFactory.soapAction + "/PRODUTOBUSCAR";

    //   if ( $scope.data.PRODUTOPESQ !== "" ) {

    //     $ionicLoading.show( {
    //       template: '<ion-spinner icon="dots"></ion-spinner>',
    //       duration: "5000"
    //     } );

    //     $http.post( url, {
    //         "IDEMPRESA": $scope.data.IDEMPRESA,
    //         "IDCONEXAO": $scope.data.IDCONEXAO,
    //         "PRODUTOPESQ": $scope.data.PRODUTOPESQ
    //       }, {
    //         'Content-Type': soapFactory.contentTypeForm,
    //         "SOAPAction": soapAction
    //       } )
    //       .success( function ( data ) {
    //         //console.log(data);
    //         for ( var i = 0; i < data.PRODUTOBUSCARRESULT.STRING.length; i++ ) {
    //           $scope.separado[ i ] = data.PRODUTOBUSCARRESULT.STRING[ i ].split( "," );
    //           var n = i - 1;
    //           $scope.separadoItem[ n ] = {
    //             cod: $scope.separado[ i ][ 0 ],
    //             nome: $scope.separado[ i ][ 1 ],
    //             unid: $scope.separado[ i ][ 2 ],
    //             outro: $scope.separado[ i ][ 3 ]
    //           };
    //         }
    //         $ionicLoading.hide();

    //         $scope.data.PRODUTOPESQ = "";
    //         //console.log( "$scope.data.PRODUTOPESQ" );
    //         //console.log( $scope.data.PRODUTOPESQ );

    //         if ( $scope.separado[ 0 ] == "S" ) {
    //           //Go
    //         } else {
    //           $scope.showAlert( "Smartsales", $scope.separado[ 1 ] );
    //         }
    //       } )
    //       .error( function ( data, status, headers, config ) {
    //         console.log( data );
    //         $ionicLoading.hide();
    //       } );
    //   } else {
    //     $scope.showAlert( 'Preste atenção', 'Por favor, insira dados no campo' );
    //   }

    //   $scope.data.PRODUTOPESQ = "";
    //   $scope.separado = [];
    // } //end listarProdutos();

    /*
     *Função que lista todas as informações básicas de um produto
     */
    function produtoEspecifico( idProduto ) {
      cotacaoFactory.produtoId = idProduto;
      //Url to to make the soap call
      var url = soapFactory.url + "/PRODUTODADOS.php";
      var soapAction = soapFactory.soapAction + "/PRODUTODADOS";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "PRODUTOCOD": idProduto
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.PRODUTODADOSRESULT.STRING.length; i++ ) {
            $rootScope.produtoData[ i ] = data.PRODUTODADOSRESULT.STRING[ i ].split( "," );
            console.log( "produtoData" + [ i ] + " = " + $rootScope.produtoData[ i ] );
            $rootScope.produtoData[ i ] = $rootScope.produtoData[ i ].toString();
          }
          $ionicLoading.hide();

          // Função para definir o preço uitário do produto
          precoUnitario( idProduto );

          //Busca Informações de Estoque Detalhada
          produtoEstoqueDetSemRE(idProduto);

          //Redireciona
          $state.go( "inserirProdutoInterna", {
            clienteId: $scope.data.clienteId,
            cotacaoId: $scope.data.cotacaoId,
            produtoId: idProduto
          } );
        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End produtoEspecifico

    /*
     *Função que lista todas as informações básicas de um produto
     */
    function produtoEspecificoSemRe( idProduto ) {
      cotacaoFactory.produtoId = idProduto;
      //Url to to make the soap call
      var url = soapFactory.url + "/PRODUTODADOS.php";
      var soapAction = soapFactory.soapAction + "/PRODUTODADOS";

      $ionicLoading.show( {
        template: '<ion-spinner icon="dots"></ion-spinner>',
        duration: "5000"
      } );

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "PRODUTOCOD": idProduto
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          console.log( data );
          for ( var i = 0; i < data.PRODUTODADOSRESULT.STRING.length; i++ ) {
            $rootScope.produtoData[ i ] = data.PRODUTODADOSRESULT.STRING[ i ].split( "," );
            console.log( "produtoData" + [ i ] + " = " + $rootScope.produtoData[ i ] );
            $rootScope.produtoData[ i ] = $rootScope.produtoData[ i ].toString();
          }
          $ionicLoading.hide();

          // Função para definir o preço uitário do produto
          precoUnitario( idProduto );

          //Busca Informações de Estoque Detalhada
          produtoEstoqueDetSemRE(idProduto);

        } )
        .error( function ( data, status, headers, config ) {
          console.log( data );
          $ionicLoading.hide();
        } );

    } //End produtoEspecificoSemRe

    /*
     *Função que acessa a cotação baseado na tabela de preços
     */
    function cotacaoTabelaPrecos() {

      var url = soapFactory.url + "/COTACAOTABELAPRECOS.php";
      var soapAction = soapFactory.soapAction + "/COTACAOTABELAPRECOS";

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.COTACAOTABELAPRECOSRESULT.STRING.length; i++ ) {
            $scope.cotTabelaPrecos[ i ] = data.COTACAOTABELAPRECOSRESULT.STRING[ i ].split( "," );
            console.log( "Tabela de precos: " + $scope.cotTabelaPrecos[ i ] );
            var n = i - 1;
            $rootScope.cotTabelPrecosSeparado[ n ] = {
              id: $scope.cotTabelaPrecos[ i ][ 0 ],
              nome: $scope.cotTabelaPrecos[ i ][ 1 ]
            };
          }

        } )

      .error( function ( data ) {
        $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
        $ionicLoading.hide();
      } );

    } //End cotacaoTabelaPrecos();

    /*
     * Função que lista o preço unitário de um produto
     */
    function precoUnitario( idProduto ) {

      var url = soapFactory.url + "/PRODUTOPRECOS.php";
      var soapAction = soapFactory.soapAction + "/PRODUTOPRECOS";
      //console.log("CotacaoDados ProdutoID: " + idProduto);

      $http.post( url, {
          "IDEMPRESA": $scope.data.IDEMPRESA,
          "IDCONEXAO": $scope.data.IDCONEXAO,
          "PRODUTOCOD": idProduto
        }, {
          'Content-Type': soapFactory.contentTypeForm,
          "SOAPAction": soapAction
        } )
        .success( function ( data ) {
          // this callback will be called asynchronously
          for ( var i = 0; i < data.PRODUTOPRECOSRESULT.STRING.length; i++ ) {
            $scope.precoUnit[ i ] = data.PRODUTOPRECOSRESULT.STRING[ i ].split( "," );
            if ( $scope.precoUnit[ 0 ] != "N" ) {
              var n = i - 1;
              $rootScope.precoUnitSeparado[ n ] = {
                id: $scope.precoUnit[ i ][ 0 ],
                nome: $scope.precoUnit[ i ][ 1 ],
                valor: $scope.precoUnit[ i ][ 2 ]
              };
              console.log( "POSICAO i: " + [ i ] );
              console.log( "$scope.precoUnit: " + $scope.precoUnit[ i ] );
              console.log( "Preco Unitario ID: " + $rootScope.precoUnitSeparado[ n ].id );
              console.log( "Preco Unitario NOME: " + $rootScope.precoUnitSeparado[ n ].nome );
              console.log( "Preco Unitario VALOR: " + $rootScope.precoUnitSeparado[ n ].valor );
            } else {
              $state.go( "inserirItensCotacao", {
                clienteId: $stateParams.clienteId,
                cotacaoId: $stateParams.cotacaoId
              } );
              //  history.back();
              $cordovaToast.showShortBottom( $scope.precoUnit[ 1 ].toString() );
            }
          } //End for
        } )
        .error( function ( data ) {
          $scope.showAlert( "Erro", "Impossivel se comunicar com o servidor!" );
          $ionicLoading.hide();
        } );

    } //End cotacaoTabelaPrecos();

    /*
     * Funçao que calcula o valor total de um produto para inserir na lista de produtos da cotação de um cliente
     */
    function calculaTotalProduto() {

      if ( $scope.cotacaoDados.produtoQuantCot && $scope.cotacaoDados.produtoPrecoUnit ) {
        var total = ( parseFloat( $scope.cotacaoDados.produtoQuantCot ) * parseFloat( $scope.cotacaoDados.produtoPrecoUnit.valor ) ).toFixed( 2 );
        total = parseFloat( total ).toFixed( 2 );

        cotacaoFactory.produtoValorTotal = total;
        $rootScope.cotacaoDados.produtoValorTotal = total;
      }

      //   var desconto = $scope.cotacaoDados.produtoDescontoPerc;
      //   if(desconto > 0){
      //     var descontoValor = (total * desconto)/100;
      //     total = parseFloat(total - descontoValor).toFixed(2);
      //     cotacaoFactory.produtoDescontoPerc = desconto;
      //     $rootScope.cotacaoDados.produtoValorTotal = desconto;
      //   }
    } // end calculaTotalProduto()

  } //End controller

} )();
