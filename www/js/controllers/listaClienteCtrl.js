( function () {
  'use strict';

  angular.module( 'smartsales.controllers.listaClienteCtrl', [ 'ngCordova', "ui.router" ] )

  /* @Controller*/
  .controller( "listaClienteCtrl", listaClienteCtrl );

  /* Injeção de dependência*/
  listaClienteCtrl.$inject = [ "$ionicPopup", "$rootScope", "$scope", "$state", "$http", "$ionicLoading", "$cordovaBarcodeScanner", "$ionicPlatform", "$stateParams", "$ionicActionSheet", "$localstorage"
  ];

  /*  Declara uma função para ser usado no controller 'produtoCtrl' e com todas as funções do $scope Cliente*/
  function listaClienteCtrl($ionicPopup, $rootScope, $scope, $state, $http, $ionicLoading, $cordovaBarcodeScanner, $ionicPlatform, $stateParams, $ionicActionSheet, $localstorage ) {

    document.addEventListener("deviceready", onDeviceReady, false);

    //Executa quando abrir a Controller
    function onDeviceReady() {

      //Google analytics
      window.analytics.startTrackerWithId('UA-40700062-2');
      window.analytics.trackView('Lista Clientes');

      //Inicia o root cliente 
      $rootScope.cliente = {
        CLIENTEPESQ: ""
      };

      $scope.showAlert = showAlert;
      $scope.separadoItem = [];
      $scope.status = "S";

    }//End onDeviceReady


    //Construção de Alert
    function showAlert( title, msg ) {
      var alertPopup = $ionicPopup.alert( {
        title: title,
        template: msg
      } );
      alertPopup.then( function ( res ) {
        console.log( 'alertPop.then' );
      } );
    }//Fim showAlert


    /*Função que lista todos os clientes baseados na pesquisa do usuário*/
    $scope.listarClientes = function() {

      $scope.separadoItem = [];

      //Valida se CLIENTEPESQ esta vazio
      if ( $scope.cliente.CLIENTEPESQ == "" ) {

        //Mensagem de erro
        $scope.showAlert('Atenção', 'Por favor, insira dados no campo!' );

      } else {

        //Mensagem após clicar em Buscar
        $ionicLoading.show( {
          template: '<ion-spinner icon="dots"></ion-spinner>',
          duration: "5000"
        } );


        //Configura variaveis do POST
        var vars = "IDEMPRESA=" + $localstorage.get("loginEMPRESASELECT") + 
                  "&IDCONEXAO=" + $localstorage.get('loginCONEXAO') +
                  "&VENDCOD=" + $localstorage.get('VENDCOD') + 
                  "&CLIENTEPESQ="+ $scope.cliente.CLIENTEPESQ;

        
        //Faz POST no WS
        $.post("http://geracaomidia.com.br/app/smartsales/CLIENTEBUSCAR.php", vars)
          .done(function(data){

            //Recebe o status de retorno
            $scope.status = data.CLIENTEBUSCARRESULT.STRING[0];

            //Valida as condições
            if ($scope.status == "S"){

              //Carrega informações de retorno
              for ( var i = 0; i < data.CLIENTEBUSCARRESULT.STRING.length; i++ ) {

                var n = i -1;

                $scope.separadoItem[n] = {
                  idCliente: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[0],
                  nome: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[1],
                  nomeCliente: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[2],
                  fantasia: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[3],
                  uf: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[4],
                  cnpj: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[5],
                  vendedor: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[6],
                  cotacaoEmAberto: data.CLIENTEBUSCARRESULT.STRING[i].split(",")[7]
                };
              }

              $ionicLoading.hide();

              $scope.cliente.CLIENTEPESQ = "";

              $cordovaToast.showShortBottom("Cliente adicionado com Sucesso!");

              $state.go("inicio");

            } else if ($scope.status == "N"){

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro do WS
              var erro = data.CLIENTEBUSCARRESULT.STRING[1];
              $cordovaToast.showShortBottom(erro);

            }//End IF Valida condição

            } )
            .fail(function(data) {

              //Fecha o Loading
              $ionicLoading.hide();

              //Mensagem de Erro
              $scope.showAlert( "Erro de conexão", "O servidor parece estar fora do ar.\n Por favor, tente novamente" );
          } );
  
          // $scope.clientes = [];
          // var busca = $scope.cliente.CLIENTEPESQ;

          // //Recebe DB
          // var clientes_ = $localstorage.get("clientesDB");

          // //Salva no array
          // $scope.clientes = JSON.parse(clientes_);

          // //Valida status
          // $scope.status = $scope.clientes.STRING[0];

          // //Valida as condições
          // if ($scope.status == "S"){

          //   //alert($scope.clientes.STRING.length);

          //   //Carrega informações de retorno
          //   for ( var i = 0; i < $scope.clientes.STRING.length; i++ ) {

          //     var nomeCliente1 = $scope.clientes.STRING[i].split(",")[1];
          //     var nomeCliente2 = $scope.clientes.STRING[i].split(",")[2];

          //     //alert($scope.clientes.STRING[i].split(",")[2]);

          //     //alert($scope.clientes.STRING[i].split(",")[1] + " ou " + nomeCliente2 + " = " + busca);

          //     if (nomeCliente1 == busca || nomeCliente2 == busca){

          //       alert("OK");

          //       var n = i -1;

          //       $scope.separadoItem[n] = {
          //         idCliente: $scope.clientes.STRING[i].split(",")[0],
          //         nome: $scope.clientes.STRING[i].split(",")[1],
          //         nomeCliente: $scope.clientes.STRING[i].split(",")[2],
          //         fantasia: $scope.clientes.STRING[i].split(",")[3],
          //         uf: $scope.clientes.STRING[i].split(",")[4],
          //         cnpj: $scope.clientes.STRING[i].split(",")[5],
          //         vendedor: $scope.clientes.STRING[i].split(",")[6],
          //         cotacaoEmAberto: $scope.clientes.STRING[i].split(",")[7]
          //       };

          //     }//End IF

          //   }//End FOR

          //   $ionicLoading.hide();

          //   $scope.cliente.CLIENTEPESQ = "";

          // }//End IF

        }//End IF

      $scope.cliente.CLIENTEPESQ = "";

    } //End listarClientes();


    $scope.salvaIDCliente = function(id,uf){


      $localstorage.set("idCliente", id);
      $localstorage.set("PRODESTADO", uf);

      //Vai para tela de menu dos clientes
      $state.go("menuClientes");

    }//End salvaIDProduto


  } //End controller

} )();
