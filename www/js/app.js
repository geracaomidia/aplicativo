( function () {
  'use strict';

  /*
   * Módulo que lista todos as dependências do aplicativo
   */
  angular.module( 'smartsales', [ 'ionic',
    "ngRoute",
    "smartsales.controllers.configCtrl", //OK
    "smartsales.controllers.loginCtrl", //OK
    "smartsales.controllers.menuCtrl", //OK
    "smartsales.controllers.listaProdutoCtrl", //OK
    "smartsales.controllers.detalheProdutoCtrl", //OK
    "smartsales.controllers.produtoCtrl", //OK
    "smartsales.controllers.listaCotacaoVendedorCtrl", //OK
    "smartsales.controllers.listaClienteCtrl", //OK
    "smartsales.controllers.menuClienteCtrl", //OK
    "smartsales.controllers.detalheClienteCtrl", //OK
    "smartsales.controllers.posicaoFinanceiraCtrl", //OK
    "smartsales.controllers.negociosCtrl", //OK
    "smartsales.controllers.addCotacaoCtrl", //OK
    "smartsales.controllers.tabelaPrecoCtrl", //OK
    "smartsales.controllers.sincronizarCtrl", //OK
    "smartsales.controllers.descontosCtrl", //OK
    "smartsales.controllers.impostoCtrl", //OK
    "smartsales.controllers.resumoCotacaoCtrl", //OK
    "smartsales.controllers.clienteCtrl", //REVISAR
    // "smartsales.controllers.cadastreSeCtrl", //REVISAR
    "smartsales.controllers.clienteAddCtrl", //REVISAR
    "smartsales.controllers.itensCotacaoCtrl", //OK
    "smartsales.controllers.inserirItemCotacaoCtrl", //OK
    "smartsales.controllers.alterarProdutoInternaCtrl", //OK
    "smartsales.controllers.actionSheetCtrl", //REVISAR
    "smartsales.controllers.actionMenuAlterarCotacao", //REVISAR
    "smartsales.services.loginFactory", //REVISAR
    "smartsales.services.cotacaoFactory", //REVISAR
    "smartsales.services.soapFactory", //REMOVER
    "smartsales.services.$localstorage", //OK
    "smartsales.config.routes"
  ] )

  .run( runIonic );

  /*
   * Função que permite o uso do ionic
   */
  function runIonic( $ionicPlatform ) {
    $ionicPlatform.ready( function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if ( window.cordova && window.cordova.plugins.Keyboard ) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar( true );
        cordova.plugins.Keyboard.disableScroll( true );
      }
      if ( window.StatusBar ) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    } );
  }

} )();
