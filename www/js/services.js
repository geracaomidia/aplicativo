angular.module('starter.services', [])

.factory('myService', function($ionicLoading, $ionicPopup) {
    return {
        showAlert: function(title, message) {
		    var alertPopup = $ionicPopup.alert({
				title: title,
				template: message
			});
		},
		showLoading: function(message) { $ionicLoading.show({ template: message }); },
		hideLoading: function(){ $ionicLoading.hide();},
    };
})