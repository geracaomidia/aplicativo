(function() {
    'use strict';

    angular.module('smartsales.services.cotacaoFactory', [])

    .factory("cotacaoFactory", cotacaoFactory);

    /*
     * Função que armazena os singletons do aplicativo
     */
    function cotacaoFactory() {

        return {
            cnpjCliente: "",
            desconto1: "",
            desconto2: "",
            desconto3: "",
            desconto4: "",
            observacao: "",
            idCliente: "",
            idCotacao: "",
            idEmpresa: "",
            idconexao: "",
            nomeCliente: "",
            produtoId: "",
            itemId: "",
            produtoNome: "",
            produtoEstoque: "",
            produtoTransito: "",
            produtoQuantDisp: "",
            produtoQuantCot: "",
            produtoPrecoUnit: "",
            produtoDescontoPerc: 0,
            produtoValorTotal: "",
            condPgmt: {
                id: null,
                nome: null
            },
            tabelaPreco: {
                id: null,
                nome: null
            },
            tipoFrete: {
                id: null,
                nome: null
            },
            transportadora: {
                id: null,
                nome: null
            },
            ufCliente: "",
            listaItensSeparado: [{
                item: "",
                idProduto: "",
                quantidade: "",
                valorTotal: "",
                descricaoProduto: ""
            }]
        };

    }

})();
