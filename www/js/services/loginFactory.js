(function() {
    'use strict';

    angular
        .module('smartsales.services.loginFactory', [])

    .factory("loginFactory", loginFactory);

    /*
     * Função que armazena os dados de login do vendedor
     */
    function loginFactory($window) {

        return {

            vendedorInformacao: [{
                idEmpresa: "",
                licenca: "",
                outro: ""
            }]
        };

    }

})();
