( function () {
  'use strict';

  angular.module( 'smartsales.services.soapFactory', [] )

  .factory( "soapFactory", soapFactory );

  /*
   * Função que armazena os tipos de dados e links do sistema em um fábrica de singleton
   */
  function soapFactory( loginFactory ) {

    return {
      url: "http://geracaomidia.com.br/app/smartsales",
      contentTypeForm: "application/x-www-form-urlencoded",
      contentTypeJson: "application/json",
      soapAction: loginFactory.servidor + ":" + loginFactory.porta || "http://focorio.ddns.net:10100" /* nome do metodo */
    };

  }

} )();
