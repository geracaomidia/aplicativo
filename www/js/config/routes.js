(function() {
    'use strict';

    angular.module('smartsales.config.routes', [])

    .config(routes);

    function routes($stateProvider, $httpProvider, $urlRouterProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        $stateProvider

            .state('login', {
            url: '/login',
            templateUrl: 'templates/loginhome.html',
            controller: 'loginCtrl'
            })

            .state('configuracao', {
            url: '/configuracao',
            templateUrl: 'templates/configuracao.html',
            controller: 'configCtrl'
            })

            .state('inicio', {
            url: '/inicio',
            templateUrl: 'templates/inicio.html',
            controller: 'menuCtrl'
            })

            .state('produtosInterna', {
            url: '/produtointerna',
            cache: false,
            templateUrl: 'templates/produtos-interna.html',
            controller: "detalheProdutoCtrl"
            })

            .state('sincronizar', {
            url: '/sincronizar',
            templateUrl: 'templates/sincronizar.html',
            controller: "sincronizarCtrl"
            })

            
            .state('produtos', {
              url: '/produtos',
              cache: false,
              templateUrl: 'templates/produtos.html',
              controller: "listaProdutoCtrl"
            })

            //  .state('menu', {
            //   url: '/menu',
            //   templateUrl: 'templates/menu.html',
            //   controller: 'menuCtrl'
            // })

            .state('listaCotacaoVendedor', {
              url: '/listaCotacaoVendedor',
              cache: false,
              templateUrl: 'templates/listaCotacaoVendedor.html',
              controller: 'listaCotacaoVendedorCtrl'
            })

            .state('clientes', {
              url: '/clientes',
              cache: false,
              templateUrl: 'templates/clientes.html',
              controller: 'listaClienteCtrl'
            })

            .state('cadastre-se', {
              url: '/cadastre-se',
              cache: false,
              templateUrl: 'templates/cadastre-se.html',
              controller: 'listaClienteCtrl'
            })

            .state('addCliente', {
              url: '/addCliente',
              templateUrl: 'templates/add-cliente.html',
              controller: 'clienteAddCtrl'
            })

            // .state('listaClientes', {
            //   url: '/listaClientes',
            //   templateUrl: 'templates/lista-clientes.html'
            // })

            .state('menuClientes', {
              url: '/menuClientes',
              cache: false,
              templateUrl: 'templates/menu-clientes.html',
              controller: "menuClienteCtrl"
            })

            .state('imposto', {
              url: '/imposto',
              cache: false,
              templateUrl: 'templates/imposto.html',
              controller: "impostoCtrl"
            })

            .state('addCotacao', {
              url: '/addCotacao/',
              cache: false,
              templateUrl: 'templates/add-cotacao.html',
              controller: "addCotacaoCtrl",
            })

            .state('detalhesCliente', {
              url: '/detalhesCliente',
              cache: false,
              templateUrl: 'templates/detalhes-cliente.html',
              controller: "detalheClienteCtrl"
            })

             .state('posicaoFinanceira', {
              url: '/posicaoFinanceira',
              cache: false,
              templateUrl: 'templates/posicao-financeira.html',
              controller: "posicaoFinanceiraCtrl"
            })

            //  .state('nfs', {
            //   url: '/nfs/:clienteId',
            //   templateUrl: 'templates/nfs.html'
            // })

            //  .state('alterarCotacao', {
            //   url: '/alterarCotacao/:clienteId/:cotacaoId',
            //   templateUrl: 'templates/alterar-cotacao.html',
            //   controller: "clienteCtrl"
            // })

            .state('alterarProdutoInterna', {
             url: '/alterarProdutoInterna',
             cache: false,
             templateUrl: 'templates/alterar-produto-interna.html',
             controller: "alterarProdutoInternaCtrl"
           })

             .state('itensCotacao', {
              url: '/itensCotacao',
              cache: false,
              templateUrl: 'templates/itens-cotacao.html',
              controller: "itensCotacaoCtrl"
            })

             .state('descontos', {
              url: '/descontos',
              templateUrl: 'templates/descontos.html',
              controller: "descontosCtrl"
            })

             .state('resumoCotacao', {
              url: '/resumoCotacao',
              templateUrl: 'templates/resumo-cotacao.html',
              controller: "resumoCotacaoCtrl"
            })

             .state('inserirItensCotacao', {
              url: '/inserirItensCotacao',
              cache: false,
              templateUrl: 'templates/inserir-itens-cotacao.html',
              controller: "inserirItemCotacaoCtrl"
            })

            //  .state('inserirProdutoInterna', {
            //   url: '/inserirProdutoInterna/:clienteId/:cotacaoId/:produtoId',
            //   templateUrl: 'templates/inserir-produtos-interna.html'
            // })

            .state('tabelaPreco', {
              url: '/tabelaPreco',
              cache: false,
              templateUrl: 'templates/tabelaPreco.html',
              controller: "tabelaPrecoCtrl"
            })

             .state('negocios', {
              url: '/negocios',
              cache: false,
              templateUrl: 'templates/negocios.html',
              controller: "negociosCtrl"
            });

          // if none of the above states are matched, use this as the fallback
          $urlRouterProvider.otherwise('/inicio');
        }

})();
